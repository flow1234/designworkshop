var express = require('express');
var app     = express();
var http    = require('http').Server(app);
var io      = require('socket.io')(http);

app.use(express.static('Controller'));

io.on('connection', function(socket)
{    
    console.log('controller connected');

    /**
        Notifies desired Gadgets to fire their notification patterns.
    */
    socket.on('notify', function(msg){
      console.log('Notification sent. Gadgets firing.\nJSON:\n'+msg);
    });

    /**
        Updates currently connected gadgets in the controller.
    */
    socket.on('update', function(msg){
      var x = 0;
      console.log('Server updated.\n'+x+'Gadgets connected.');
      console.log('JSON:\n'+msg);
    });

    // use io.sockets.emit to push messages to ALL sockets (socket.emit just pushes messages back)!!!

    socket.on('test', function(msg){
      console.log("emitted");
      io.sockets.emit('server push to controller',msg);
    });

    socket.on('controller push', function(msg){
      //socket.emit('ctrl push', msg);
      console.log("CONTROLLER PUSH");
      console.log(msg);
      io.sockets.emit('server pull to client',"GIVE ME GADGETS");
    });

    /**
        Updates currently connected gadgets in the controller.
    */
    socket.on('controller pull', function(msg){
      //TESTCASE START
      glist = [];
      glist.push({
            gadget: "vibro01"
      });
      glist.push({
            gadget: "vibro02"
      });
      glist.push({
            gadget: "ring01"
      });
      glist.push({
            gadget: "leds03"
      });
      //TESTCASE END

      console.log("CONTROLLER PULL");
      socket.emit('server push to controller',glist);
    });

    /**
        Controller -> Smartphone
        Request active gadgets.
    */
    socket.on('AB_request', function(msg){
      //TODO
      console.log("AB_request\n");
      io.sockets.emit('BC_request',msg);
    });

    /**
        Controller -> Smartphone
        Send notifications to active gadgets.
    */
    socket.on('AB_update', function(msg){
      //TODO
      console.log("AB_update");
      console.log("Active Gadgets:\n"+msg+"\n")
      io.sockets.emit('BC_update',msg);
    });

    /**
        Smartphone -> Controller
        Send active gadgets.
    */
    socket.on('CB_update', function(msg){
      //TODO
      console.log("CB_update");
      console.log("Active Gadgets:\n"+msg+"\n");
      io.sockets.emit('BA_update',msg);
    });


});

http.listen(3000, function() {
  console.log('listening on *:3000');
});