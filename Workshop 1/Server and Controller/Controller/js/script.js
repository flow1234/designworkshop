var socket; // Socket for communication
var allGadgets, activeGadgets; // JSON containing all gadgets and Array with all active Gadgets
var ids = ["g01","g02","g03","g04","g05","g06","g07","g08","g09","g10","g11","g12","g13","g14","g15","g16"];

window.onload = function() 
{
    socket = io();
    activeGadgets = [];
    
    // Suppress ajax warning for loading json on page load
    $.ajaxSetup({beforeSend: function(xhr)
    {
        if (xhr.overrideMimeType)
        {
            xhr.overrideMimeType("application/json");
        }
    }});
    
    // Load list of all gadgets
    $.getJSON( "gadgets.json", 
    {
        format: "json"
    }).done(function(data) 
    {
        console.log("Available Gadgets JSON loaded");
        allGadgets = $.extend(true, {}, data);
        update();
    });

    // Add listeners to disabled checkboxes, if gadget is disabled -> background red, else -> green
    // Also add listeners to pattern dropdowns.
    for (i=0;i<ids.length;i++)
    {
        $('#'+ids[i]+'-checkbox').change(function() 
        {
            if($(this).is(":checked"))
            {
                $('#'+$(this).attr("id").substring(0, 3)+'-ctr').parent().parent().css( "background", "red" );
            }
            else
            {
                $('#'+$(this).attr("id").substring(0, 3)+'-ctr').parent().parent().css( "background", "green" );
            }
        });

        $('#'+ids[i]+'-select').change(function() 
        {
            for (j=0;j<activeGadgets.length;j++)
            {
                if (ids[i] == activeGadgets[j].id)
                {
                    activeGadgets[j].pattern = $('option:selected', this).val();
                    console.log(activeGadgets[j].pattern);
                    console.log($('option:selected', this).val());
                }
            }
        });
    }

    // On call from server. Update list of gadgets.
    socket.on('server push to controller', function(msg) {
        console.log(msg);
        console.log(JSON.parse(msg));
        update(JSON.parse(msg));
    });

    // On call from server. Update list of gadgets.
    socket.on('BA_update', function(msg) {
        console.log(JSON.parse(msg));
        update(JSON.parse(msg));
    });
}


/**
    Send notification to server of specified active gadget.

    @param: id - String
*/
function notify(id)
{
    //console.log(id.substring(0,3));
    for (i=0;i<activeGadgets.length;i++)
    {
        if (id.substring(0,3) == activeGadgets[i].id)
        {
            if (! $('#'+id.substring(0,3)+'-checkbox').is(':checked'))
            {
                activeGadgets[i].pattern = $('#'+id.substring(0,3)+'-select').val();
                console.log(JSON.stringify(activeGadgets[i]));
                socket.emit('AB_update',JSON.stringify(activeGadgets[i]));
            }
        }
    }
}


/**
    Send notification to server of all active gadgets.
*/
function notifyAll()
{
    for (j=0;j<ids.length;j++)
    {
        for (i=0;i<activeGadgets.length;i++)
        {
            if (ids[j] == activeGadgets[i].id)
            {
                activeGadgets[i].pattern = $('#'+ids[j]+'-select').val();

                if ($('#'+ids[j]+'-checkbox').is(':checked'))
                {
                    activeGadgets[i].disabled = true;
                }
                else
                {
                    activeGadgets[i].disabled = false;
                }
            }
        }
    }

    console.log(JSON.stringify(activeGadgets));
    socket.emit('AB_update',JSON.stringify(activeGadgets));
}


/**
    Request an update of active gadgets from the server.
*/
function updateGadgets()
{
    socket.emit('AB_request');
}


/**
    Set all connected gadgets to enabled.
*/
function enableAll()
{
    for (i=0;i<ids.length;i++)
    {
        $('#'+ids[i]+'-checkbox').prop('checked', false).checkboxradio('refresh');
        $('#'+ids[i]+'-ctr').parent().parent().css( "background", "green" );
    }
    console.log("All connected gadgets are enabled");
}


/**
    Set all connected gadgets to disabled.
*/
function disableAll()
{
    for (i=0;i<ids.length;i++)
    {
        $('#'+ids[i]+'-checkbox').prop('checked', true).checkboxradio('refresh');
        $('#'+ids[i]+'-ctr').parent().parent().css( "background", "red" );
    }
    console.log("All connected gadgets are disabled");
}


/**
    Reset all patterns of active gadgets.
*/
function resetPatterns()
{
    for (i=0;i<ids.length;i++)
    {
        $('#'+ids[i]+'-select').val(1);
    }
    $('select').selectmenu('refresh');
}






/**
    Update the controller view. Receive a JSON Object with the ids of all plugged in gadgets.
    Dynamically adjust the list based on active devices. The Smartphone and the projector 
    are always active, since they are (1) required and (2) don't draw any current from the plug.
    If there's no list, reload all the gadgets.

    @param: gadgetsNew - JSON Object [{id},{id},{id}]
*/
function update(gadgetsNew)
{
    /*
        logic for updating gadget list here
        Access JSON like this: allGadgets[0].key
        Dictionary: activeGadgets.push({id:ids[i],gadget:allGadgets[i].id});
        Access Dictionary: activeGadgets[0].id + " " + activeGadgets[0].gadget
    */

    var updateList;
    var gadgetCounter = 0;
    var amps_used = 0;
    activeGadgets = [];

    // if update is called without plugged gadgets
    if (gadgetsNew == null)
    {
        updateList = allGadgets;
    }
    else
    {
        updateList = [];

        // Push smartphone and projector (always active and no current drawn)
        for (j=0;j<2;j++)
        {
            updateList.push({
                id:                allGadgets[j].id,
                imagesource:       allGadgets[j].imagesource,
                description:       allGadgets[j].description,
                currentUsage:      allGadgets[j].currentUsage,
                availablePatterns: allGadgets[j].availablePatterns
            });
        }


        for (i=0;i<gadgetsNew.length;i++)
        {
            for (j=0;j<Object.keys(allGadgets).length;j++)
            {
                if (allGadgets[j].id == gadgetsNew[i].gadget)
                {
                    updateList.push({
                        id:                gadgetsNew[i].gadget,
                        imagesource:       allGadgets[j].imagesource,
                        description:       allGadgets[j].description,
                        currentUsage:      allGadgets[j].currentUsage,
                        availablePatterns: allGadgets[j].availablePatterns
                    });
                }
            }
        }
    }

    for (i=0;i<ids.length;i++)
    {
        document.getElementById("gadgetList").children[i+2].style.display = "block";
    }

    // recieve active gadgets
    for (i=0;i<Object.keys(updateList).length;i++)
    {
        activeGadgets.push({
            id:       ids[i],
            gadget:   updateList[i].id,
            pattern:  1,
            distance: 1,
            severity: 1,
            disabled: false});
    }

    // update list based on active gadgets
    for (i=0;i<activeGadgets.length;i++)
    {
        gadgetCounter++;
        amps_used += updateList[i].currentUsage;

        // update controller view
        $('#'+activeGadgets[i].id+'-ctr').text('Gadget ' + gadgetCounter);
        $('#'+activeGadgets[i].id+'-ctr').parent().parent().css( "background", "green" );
        $('#'+activeGadgets[i].id+'-img').attr('src', './images/'+updateList[i].imagesource);
        $('#'+activeGadgets[i].id+'-descr').text(updateList[i].description);
        $('#'+activeGadgets[i].id+'-test').text(updateList[i].currentUsage);
        $('#'+activeGadgets[i].id+'-checkbox').prop('checked', false).checkboxradio('refresh');

        // update patterns
        var myOptions = [];
        var mySelect = $('#'+activeGadgets[i].id+'-select');
        var patterns = 0;
        
        mySelect.empty();

        for (j=0;j<Object.keys(updateList).length;j++)
        {
            if (updateList[j].id == activeGadgets[i].gadget)
            {
                patterns = updateList[j].availablePatterns;
            }
        }

        // populate patterns
        for (j=0;j<allGadgets[i].availablePatterns;j++)
        {
            myOptions.push({
                val:  j+1,
                text: "Pattern " + (j+1)
            });    
        }

        // refill pattern-dropdowns
        for (j=0;j<myOptions.length;j++)
        {
            mySelect.append($('<option></option>').val(myOptions[j].val).html(myOptions[j].text));
        }
        $('select').selectmenu('refresh');
    }

    // remove and disable unused items
    for (i=ids.length;i>activeGadgets.length;i--)
    {
        if (Object.keys(updateList).length == 15) console.log("ALL GADGETS");
        document.getElementById("gadgetList").children[i+1].style.display = "none";
        $('#'+ids[i+1]+'-checkbox').prop('checked', true).checkboxradio('refresh');
    }

    amps_used /= 1000;

    $("#totalamps").text(amps_used);

    if (amps_used<2)
    {
        $("#totalamps_color").css('color','green');
    }
    else if (amps_used<3)
    {
        $("#totalamps_color").css('color','rgb(230, 138, 0)');
    }
    else 
    {
        $("#totalamps_color").css('color','red');
    }
}


/**
    Test function
*/
function test()
{
    alert("no test case specified");
}