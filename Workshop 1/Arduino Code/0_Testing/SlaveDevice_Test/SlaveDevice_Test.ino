// Include the required Wire library for I2C<br>
#include <Wire.h>
#include <ArduinoJson.h>

#define WIRE_ID 5

int LED = 9;
int x = 0;
String c ="";
volatile char buffer[40];
volatile boolean receiveFlag = false;

void setup() {
  // Define the LED pin as Output
  pinMode (LED, OUTPUT);
  Serial.begin(9600);
  Wire.begin(WIRE_ID);
  Serial.print("Slave connected on ");
  Serial.println(WIRE_ID);
  // Attach a function to trigger when something is received.
  Wire.onReceive(receiveEvent);
}

void loop() {
}

void test() {
}


void receiveEvent(int bytes) {
  int ctr = 0;
  //Serial.println("SHIT RECEIVED");
  while (0 < Wire.available()) { // loop through all bytes
    char c = Wire.read();
    //Serial.println(c); 
    if (ctr==2) {
     Serial.print("Pattern: ");
     Serial.print(c);
    }
    if (ctr==3) {
      Serial.print(" Severity: ");
      Serial.print(c);
    }
    if (ctr==4) {
      Serial.print(" Distance: ");
      Serial.println(c);
    }
    ctr++;
    
    //char c = Wire.read();        // receive byte as a character
    //Serial.println(c);           // print the character
  }
  digitalWrite(LED, HIGH);
  delay(2000);
  digitalWrite(LED, LOW);
}
