// Arduino Pro Mini PWM-Pins
int MOTOR_1 = 3;
int MOTOR_2 = 5;
int MOTOR_3 = 9;

/* Arduino UNO PWM-Pins
int MOTOR_1 = 9;
int MOTOR_2 = 10;
int MOTOR_3 = 11;
*/
void setup() {
  // put your setup code here, to run once:
  pinMode (MOTOR_1, OUTPUT);
  pinMode (MOTOR_2, OUTPUT);
  pinMode (MOTOR_3, OUTPUT);
}

void loop() {
  triggerGadget('1','x','x');
  delay(5000);
  triggerGadget('2','x','x');
  delay(5000);
  triggerGadget('3','x','x');
  delay(5000);
  triggerGadget('4','x','x');
  delay(5000);
  triggerGadget('5','x','x');
  delay(5000);
}

/**
 * @param pattern  - pre-defined for each gadget
 * @param distance - Not required for test - '1': 50m , '2': 10m (time to travel 40m at 20kmh: 7s)
 * @param severity - Not required for test - '1': light, '2': medium, '3': severe
 */
void triggerGadget(char pattern, char distance, char severity) {
  
  switch (pattern){
    
    case '1': // constant vibrations at 100%
    
      analogWrite( MOTOR_1 , 153 );  // 60% duty cycle ~ 100% of motor
      analogWrite( MOTOR_2 , 153 );  // 60% duty cycle ~ 100% of motor
      analogWrite( MOTOR_3 , 153 );  // 60% duty cycle ~ 100% of motor
      delay(3000);                   // play for 1s
    
      analogWrite( MOTOR_1 , 0 );    // 0% duty cycle (off)
      analogWrite( MOTOR_2 , 0 );    // 0% duty cycle (off)
      analogWrite( MOTOR_3 , 0 );    // 0% duty cycle (off)
      
      break;

      
    case '2': // constant vibrations at 33%
    
      analogWrite( MOTOR_1 , 50 );  // 30% duty cycle ~ 33% of motor
      analogWrite( MOTOR_2 , 50 );  // 30% duty cycle ~ 33% of motor
      analogWrite( MOTOR_3 , 50 );  // 30% duty cycle ~ 33% of motor
      delay(3000);                   // play for 1s
    
      analogWrite( MOTOR_1 , 0 );    // 0% duty cycle (off)
      analogWrite( MOTOR_2 , 0 );    // 0% duty cycle (off)
      analogWrite( MOTOR_3 , 0 );    // 0% duty cycle (off)
      
      break;

      
    case '3': // ramping vibrations from 0% to 100%

      for (int i=0;i<154;i++){
        analogWrite( MOTOR_1 , i );  // 60% duty cycle ~ 0%->100% of motor
        analogWrite( MOTOR_2 , i );  // 60% duty cycle ~ 0%->100% of motor
        analogWrite( MOTOR_3 , i );  // 60% duty cycle ~ 0%->100% of motor
        delay(20); // 3000ms/(153)iterations = 20 ~ 3s
      }
      //delay(1000);                   // play for 1s

      analogWrite( MOTOR_1 , 0 );    // 0% duty cycle (off)
      analogWrite( MOTOR_2 , 0 );    // 0% duty cycle (off)
      analogWrite( MOTOR_3 , 0 );    // 0% duty cycle (off)
      
      break;
      
    case '4': // cycling vibrations fast ( 14 ticks/s )
    
      for (int i=0;i<15;i++){
        analogWrite( MOTOR_1 , 153 );  // 60% duty cycle ~ 100% of motor
        analogWrite( MOTOR_2 , 153 );  // 60% duty cycle ~ 100% of motor
        analogWrite( MOTOR_3 , 153 );  // 60% duty cycle ~ 100% of motor
        delay(100); // 15*100ms = 1.5s
        analogWrite( MOTOR_1 , 0 );    // 0% duty cycle (off)
        analogWrite( MOTOR_2 , 0 );    // 0% duty cycle (off)
        analogWrite( MOTOR_3 , 0 );    // 0% duty cycle (off)
        delay(100); // 15*100ms = 1.5s -> 2*1.5s = 3s
      }
      
      break;
    
    case '5': // cycling vibrations slow ( 7 ticks/s )
      
      for (int i=0;i<7;i++){
        analogWrite( MOTOR_1 , 153 );  // 60% duty cycle ~ 100% of motor
        analogWrite( MOTOR_2 , 153 );  // 60% duty cycle ~ 100% of motor
        analogWrite( MOTOR_3 , 153 );  // 60% duty cycle ~ 100% of motor
        delay(200); // 7*200ms = 1.4s
        analogWrite( MOTOR_1 , 0 );    // 0% duty cycle (off)
        analogWrite( MOTOR_2 , 0 );    // 0% duty cycle (off)
        analogWrite( MOTOR_3 , 0 );    // 0% duty cycle (off)
        delay(200); // 7*200ms = 1.4s -> 2*1.4s = 2.8s
      }
      
      break;

      
    default:
    
      break;
  }
}

