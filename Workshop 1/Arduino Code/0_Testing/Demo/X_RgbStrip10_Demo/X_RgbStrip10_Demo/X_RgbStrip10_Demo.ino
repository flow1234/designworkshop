#include <Adafruit_NeoPixel.h>
#include <Wire.h>

#define WIRE_ID 12 // rgbStrip01

#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 6

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(10, PIN, NEO_GRB + NEO_KHZ800);

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

void setup() {
  Serial.begin(9600);
  Wire.begin(WIRE_ID);
  Wire.onReceive(receiveEvent); // Attach a function to trigger when something is received.
  
  // LOG
  Serial.print("Slave connected on ");
  Serial.println(WIRE_ID);

  // Init gadget
  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
  #if defined (__AVR_ATtiny85__)
    if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
  #endif
  // End of trinket special code
  Serial.println("Strip Connected");


  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

void loop() {
}

/**
 * On Recieve Function
 */
void receiveEvent(int bytes) {
  int ctr = 0;
  char p, d, s;
  
  while (0 < Wire.available()) { // loop through all bytes
    char c = Wire.read();
    if (ctr==2){p = c;}
    if (ctr==3){d = c;}
    if (ctr==4){s = c;}
    ctr++;
  }
  
  //LOG
  Serial.println("Call recieved from Master Device:");
  Serial.print("Pattern: ");
  Serial.println(p);
  Serial.print("Distance: ");
  Serial.println(d);
  Serial.print("Severity: ");
  Serial.println(s);
  
  triggerGadget(p,d,s);
}

/**
 * @param pattern  - pre-defined for each gadget
 * @param distance - Not required for test - '1': 50m , '2': 10m (time to travel 40m at 20kmh: 7s)
 * @param severity - Not required for test - '1': light, '2': medium, '3': severe
 */
void triggerGadget(char pattern, char distance, char severity) {
  
  switch (pattern){
    
    case '1': // on

      colorWipe(strip.Color(255, 0, 0), 0); // Red
      delay(3000);
      colorWipe(strip.Color(0, 0, 0), 0);   // off
      
      break;

      
    case '2': // blinking fast
    
      for (int i=0;i<15;i++){
        colorWipe(strip.Color(255, 0, 0), 0); // Red
        delay(100);
        colorWipe(strip.Color(0, 0, 0), 0);   // off
        delay(100);
      }
      
      break;

      
    case '3': // blinking slow

      for (int i=0;i<4;i++){
        colorWipe(strip.Color(255, 0, 0), 0); // Red
        delay(350);
        colorWipe(strip.Color(0, 0, 0), 0);   // off
        delay(350);
      }
      
      break;

    case '4': // pulse animation

      for (int j=0;j<3;j++){
        for (int i=0;i<255;i+=5){
          colorWipe(strip.Color(i, 0, 0), 0); // Red
          delay(5);
        }
        for (int i=255;i>0;i-=5){
          colorWipe(strip.Color(i, 0, 0), 0); // Red
          delay(5);
        }
      }
      colorWipe(strip.Color(0, 0, 0), 0);   // off
      
      break;
      
      
    case '5': // theater blinking

      for (int i=0;i<2;i++){
        theaterChase(strip.Color(127, 0, 0), 50); // Red
      }
      colorWipe(strip.Color(0, 0, 0), 0);   // off
      
      break;
      
    
    case '6': // directional animation
      
      for (int i=0;i<3;i++){
        colorWipe(strip.Color(255, 0, 0), 50); // Red
        colorWipe(strip.Color(0, 0, 0), 50);   // off
      }
      
      break;


    case '7': // color demonstration
      
      for (int i=0;i<2;i++){
        rainbow(5);
      }
      colorWipe(strip.Color(0, 0, 0), 0);   // off
      
      break;


    default:
    
      break;
  }
}

// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}

void rainbow(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel((i+j) & 255));
    }
    strip.show();
    delay(wait);
  }
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
    }
    strip.show();
    delay(wait);
  }
}

//Theatre-style crawling lights.
void theaterChase(uint32_t c, uint8_t wait) {
  for (int j=0; j<10; j++) {  //do 10 cycles of chasing
    for (int q=0; q < 3; q++) {
      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, c);    //turn every third pixel on
      }
      strip.show();

      delay(wait);

      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
}

//Theatre-style crawling lights with rainbow effect
void theaterChaseRainbow(uint8_t wait) {
  for (int j=0; j < 256; j++) {     // cycle all 256 colors in the wheel
    for (int q=0; q < 3; q++) {
      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, Wheel( (i+j) % 255));    //turn every third pixel on
      }
      strip.show();

      delay(wait);

      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}
