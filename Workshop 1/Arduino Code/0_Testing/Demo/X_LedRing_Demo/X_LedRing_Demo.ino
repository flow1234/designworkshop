#include <Adafruit_NeoPixel.h>
#include <Wire.h>

#define WIRE_ID 9 // ring01

#define PIN 6
 
// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(16, PIN, NEO_RGBW + NEO_KHZ800);


void setup() {
  Serial.begin(9600);
  Wire.begin(WIRE_ID);
  Wire.onReceive(receiveEvent); // Attach a function to trigger when something is received.
  
  // LOG
  Serial.print("Slave connected on ");
  Serial.println(WIRE_ID);

  // Init gadget
  strip.begin();
  strip.setBrightness(30); //adjust brightness here
  strip.show(); // Initialize all pixels to 'off'
}
 
void loop() {
}

/**
 * On Recieve Function
 */
void receiveEvent(int bytes) {
  int ctr = 0;
  char p, d, s;
  
  while (0 < Wire.available()) { // loop through all bytes
    char c = Wire.read();
    if (ctr==2){p = c;}
    if (ctr==3){d = c;}
    if (ctr==4){s = c;}
    ctr++;
  }
  
  //LOG
  Serial.println("Call recieved from Master Device:");
  Serial.print("Pattern: ");
  Serial.println(p);
  Serial.print("Distance: ");
  Serial.println(d);
  Serial.print("Severity: ");
  Serial.println(s);
  
  triggerGadget(p,d,s);
}

/**
 * @param pattern  - pre-defined for each gadget
 * @param distance - Not required for test - '1': 50m , '2': 10m (time to travel 40m at 20kmh: 7s)
 * @param severity - Not required for test - '1': light, '2': medium, '3': severe
 */
void triggerGadget(char pattern, char distance, char severity) {
  
  switch (pattern){
    
    case '1': // constant red light

      colorWipe(strip.Color(0,255,0),0);
      delay(3000);
      colorWipe(strip.Color(0,0,0),0);
      
      break;

      
    case '2': // rainbow red slow
    
      for (int i=0;i<3;i++){
        colorWipe(strip.Color(0, 255, 0), 40); // Green
        colorWipe(strip.Color(0, 0, 0), 40); // Green
      }
      
      break;

      
    case '3': // rainbow red fast

      for (int i=0;i<6;i++){
        colorWipe(strip.Color(0, 255, 0), 20); // Green
        colorWipe(strip.Color(0, 0, 0), 20); // Green
      }
      
      break;

      
    case '4': // blinking slow
    
      for (int i=0;i<3;i++){
        colorWipe(strip.Color(0,255,0),0);
        delay(500);
        colorWipe(strip.Color(0,0,0),0);
        delay(500);  
      }
      
      break;

    
    case '5': // blinking fast
      
      for (int i=0;i<6;i++){
        colorWipe(strip.Color(0,255,0),0);
        delay(250);
        colorWipe(strip.Color(0,0,0),0);
        delay(250);  
      }
      
      break;

    case '6': // indication right

      for (int i=0;i<3;i++){
        indicationRight(strip.Color(0, 255, 0), 20); // Red
        delay(250);
        colorWipe(strip.Color(0,0,0),0);
        delay(250);
      }

      break;


    case '7': // color switch

      for (int i=0;i<2;i++){
        colorWipe(strip.Color(255, 0, 0), 20); // Green
        colorWipe(strip.Color(0, 0, 0), 20); // Off
        colorWipe(strip.Color(255, 255, 0), 20); // Yellow
        colorWipe(strip.Color(0, 0, 0), 20); // Off
        colorWipe(strip.Color(0, 255, 0), 20); // Red
        colorWipe(strip.Color(0, 0, 0), 20); // Off
        delay(1000);
      }

      break;

      
    default:
    
      break;
  }
}

void indicationRight(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<(strip.numPixels()/2); i++) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}
 
// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}
