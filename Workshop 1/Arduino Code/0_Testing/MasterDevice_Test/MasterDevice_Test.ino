#include <Wire.h>

#define MAX_SLAVE_NODES 14

// Variables to store slave devices

byte slaveCount = 0;              // Count of active gadgets
boolean slaves[MAX_SLAVE_NODES];  // List to store active gadgets


// Variables to store incoming message from Android device

char inData[5];                   // Allocate some space for the string
char inChar=-1;                   // Where to store the character read
byte index = 0;                   // Index into array; where to store the character
boolean msgProcessed = false;     // Flag if the Android message was processed

/**
 * Arduino Init
 */
void setup() {
  Serial.begin(9600); // Start serial at 9600 baud
  Wire.begin(); // Start the I2C Bus as Master
  /* DEBUG
  Serial.println("Master-Device connected"); */

  scanner(); // scan for available gadgets
}


/**
 * Arduino Loop
 */
void loop() {
  
  if(!msgProcessed){readAndroid();delay(2000);}//delay(4000);
  //Serial.flush();
  //Serial.print(" - Serial closed ");
  //Serial.begin(9600);
  //Serial.println("Serial re-opened");
  //delay(500);
  //test();
  delay(5000);
  /*
  //input = Serial.read();

  //sendBroadcast();
  //delay(2000);
  
  //if(!msgProcessed){sendBroadcast("00342");delay(5000);}
  //if(!msgProcessed){notify("19212");delay(5000);}
  
  if(Comp("ledon")==0){
   Serial.write("LED Turned On\n");
   notify(9,"test");
   delay(1000);
   //digitalWrite(led, HIGH);
  }
 if(Comp("ledoff")==0){
   Serial.write("LED Turned Off\n");
   //digitalWrite(led, LOW);
  }*/
}

void test(){
  //char* updateGadgets = "1, 2, 3, 4, 8, ";
  String updateGadgets = "";
  for (int i=0;i<MAX_SLAVE_NODES;i++)
   {
    if (slaves[i] == 0)
    {
      updateGadgets += String(i,DEC);
      updateGadgets += ", ";
      //Serial.print(i);
      //Serial.println (" connected");
    } // end if
  } // end of for loop
  
  Serial.print("Old List: ");
  Serial.println(updateGadgets);
  //Serial.print("Strlen: ");
  //Serial.println(strlen(updateGadgets));
  //updateGadgets[strlen(updateGadgets)-2] = 0;
  Serial.print("New List: ");
  Serial.println(updateGadgets.substring(0,updateGadgets.length()-2));
}


/* 
 * =============== Android functions ===============
 */

void readAndroid(){
  if(Serial.available())
  {
    //Serial.print("if Serial available: ");
    //Serial.println(Serial.available());
   while(Serial.available() > 0) // Don't read unless
  {
    //Serial.print("while Serial available > 0: ");
    //Serial.println(Serial.available());
   if(index < 5) // One less than the size of the array
   {
     inChar = Serial.read(); // Read a character
     inData[index] = inChar; // Store it
     index++; // Increment where to write next
     inData[index] = '\0'; // Null terminate the string
     //Serial.print("if index <5: ");
     //Serial.println(inChar);
   }
  }
  index = 0;
  processAndroid(inData);
  }
}

/**
 * 
 */
void processAndroid(char* This){
  msgProcessed = true;
  Serial.print("Process Android: ");
  Serial.println(This);
  if(This[0]=='0'){ // Broadcast
    sendBroadcast(This);
    /* DEBUG
    Serial.println("Broadcast");
    Serial.print("Pattern Distance Severity: ");
    Serial.print(This[2]);
    Serial.print(This[3]);
    Serial.println(This[4]);*/
  }
  else if(This[0]=='s'){ // Re-run Scanner
    scanner();
  }
  else if(This[0]=='1'){ // Specific gadget
    notify(This);
    /* DEBUG
    Serial.print("Gadget: ");
    Serial.println(This[1]);    
    Serial.print("Pattern Distance Severity: ");
    Serial.print(This[2]);
    Serial.print(This[3]);
    Serial.println(This[4]);
    //Serial.println("Notify gadget %c, Pattern %c, Distance %c, Severity %c",gadgetID,pattern,distance,severity);*/
  }

  /* Debug
  Serial.print("Type: ");
  Serial.println(type);
  Serial.print("Gadget: ");
  Serial.println(gadgetID);
  Serial.print("Pattern: ");
  Serial.println(pattern);
  Serial.print("Distance: ");
  Serial.println(distance);
  Serial.print("Severity: ");
  Serial.println(severity);
  Serial.println("");
  */
  msgProcessed=false;
}


/**
 * Function to compare incoming messages on the Serial line (from the Smartphone).
 * 
 * Example message: 1A131
 * 0 - Broadcast , 1 - Single Gadget
 * 0 - Broadcast , 1:D - Gadget ID
 * 1:3 - Pattern
 * 1:3 - Distance
 * 1:3 - Severity
 *
char Comp(char* This){
  while(Serial.available() > 0) // Don't read unless
  {
    if(index < 19) // One less than the size of the array
    {
      inChar = Serial.read(); // Read a character
      inData[index] = inChar; // Store it
      index++; // Increment where to write next
      inData[index] = '\0'; // Null terminate the string
    }
  }

  if(strcmp(inData,This)  == 0){
    for(int i=0;i<19;i++){
      inData[i]=0;
    }
    index=0;
    return(0);
  }
  else{
    return(1);
  }
}*/


/* 
 * =============== I2C functions ===============
 */

/** 
 * Scan for all available gadgets on the bus. 
 */
void scanner() {
  // Scan for available slave-nodes
  slaveCount = 0;
  Serial.println ("I2C scanner. Scanning ...");
  for (int i=0;i<MAX_SLAVE_NODES;i++)
  {
    slaves[i]=false;
  } // end of for loop
   for (byte i = 1; i < MAX_SLAVE_NODES; i++) // total number of slaves being tested
   {
     Wire.beginTransmission (i);
     if (Wire.endTransmission () == 0)
       {
       slaves[i]=true;
       // DEBUG
       Serial.print ("Found address: ");
       Serial.print (i, DEC);
       Serial.print (" (0x");
       Serial.print (i, HEX);
       Serial.println (")");
       //
       slaveCount++;
       } // end of good response
      delay (5);  // give devices time to recover
   } // end of for loop
   // DEBUG
   Serial.println ("Done.");
   Serial.print ("Found ");
   Serial.print (slaveCount, DEC);
   Serial.println (" slave(s).");
   
   for (int i=0;i<MAX_SLAVE_NODES;i++)
   {
    if (slaves[i] != 0)
    {
      Serial.print("Slave ");
      Serial.print(i);
      Serial.println (" connected");
    } // end if
  } // end of for loop
  //


  Serial.println("DEBUG: ACTIVE GADGETS");
  
  String updateGadgets = "";
  for (int i=0;i<MAX_SLAVE_NODES;i++)
   {
    if (slaves[i] != 0)
    {
      updateGadgets += String(i,DEC);
      updateGadgets += ", ";
      //Serial.print(i);
      //Serial.println (" connected");
    } // end if
  } // end of for loop
  
  //Serial.print("Old List: ");
  //Serial.println(updateGadgets);
  //Serial.print("Strlen: ");
  //Serial.println(strlen(updateGadgets));
  //updateGadgets[strlen(updateGadgets)-2] = 0;
  Serial.print("ActiveGadgets: ");
  Serial.println(updateGadgets.substring(0,updateGadgets.length()-2));
}


/**
 * Notify all connected gadgets.
 */
void sendBroadcast(char* msg) {
  /* DEBUG
  Serial.println("BROADCAST"); 
  String message="";
  message+=msg[2];
  message+=msg[3];
  message+=msg[4]; */
  for (int i=0;i<MAX_SLAVE_NODES;i++)
  {
    if (slaves[i] == 1){
      Serial.print("Slave ");
      Serial.print(i);
      Serial.println(" connected");
      notify(i,msg);
      /* DEBUG
      Serial.print("SLAVE connected");
      Serial.print(i+1); */
    }
  }
  /*
  long timestamp_start = millis();
  Serial.println(timestamp_start);
  for (byte i=0;i<MAX_SLAVE_NODES;i++)
  {
    long timestamp_now = millis();
    long delay = timestamp_now-timestamp_start;
    Serial.print(delay);
    
    //Serial.print(millis()); // can show delay
    
    Serial.print(" ");
    Serial.print(i);

    
    //Serial.print((i+1),HEX);
    //Serial.println(message);
    
    if (slaves[i] == 1) {
      Serial.print(" ");
      Serial.print(i);
      
      notify(i,msg);

      Serial.println(" - Message sent: ");
      Serial.println(msg);
    }
  }*/
}

/**
 * Notify a specific gadget.
 * 
 *  Gadget-IDs:
 *  0 - Master
 *  1 - led01
 *  2 - led02
 *  3 - led03
 *  4 - led04
 *  5 - vibro01
 *  6 - vibro02
 *  7 - vibro03
 *  8 - vibro04
 *  9 - ring01
 *  A - lumiTape01
 *  B - lumiTape02
 *  C - rgbStrip01
 *  D - rgbStrip02
 */
void notify(int id, char* msg) {
  //String gadgetID = String(id, HEX);
  Serial.print("Notify Slave ");
  Serial.print(id);
  Wire.beginTransmission((byte)id);
  //Serial.println(msg);
  Wire.write(msg);
  Wire.endTransmission();
  /* DEBUG
  Serial.print("GADGET: ");
  Serial.print(id);
  String message="";
  message+=msg[2];
  message+=msg[3];
  message+=msg[4];
  Serial.print(" Message: ");
  Serial.println(message);
  */
}

void notify(char* msg){
  //Serial.print("NOTIFY something - ");
  //Serial.print(msg[1]);
  //Serial.print(" - Turns to Byte: ");
  //Serial.println(hexCharToByte(msg[1]));
  /* DEBUG
  char s = "";
  //int f = 0;
  //Serial.println(s);
  //Serial.println(f);
  if(msg[1]=='A') s='10';
  else if(msg[1]=='B')s='11';
  else if(msg[1]=='C')s='12';
  else if(msg[1]=='D')s='13';
  else if(msg[1]=='E')s='14';
  else if(msg[1]=='F')s='15';
  else s=msg[1];
  int f = s - '0';
  //Wire.beginTransmission(msg[1]);
  //Serial.println(f);
  Wire.beginTransmission((byte)f);*/
  //Serial.print("Notify Slave ");
  //Serial.println(msg);
  Wire.beginTransmission(hexCharToByte(msg[1]));
  //Serial.println(msg);
  Wire.write(msg);
  Wire.endTransmission();
  
  /* DEBUG 
  Serial.println("SINGLE");
  Serial.print("GADGET: ");
  if(msg[1]=='A')Serial.print(10);
  else if(msg[1]=='B')Serial.print(11);
  else if(msg[1]=='C')Serial.print(12);
  else if(msg[1]=='D')Serial.print(13);
  else if(msg[1]=='E')Serial.print(14);
  else if(msg[1]=='F')Serial.print(15);
  else Serial.print(msg[1]);
  //Serial.print(x);//(int) strtol(str, 0, 16)
  Serial.print(" Message: ");
  Serial.println(message);
  */
}

byte hexCharToByte(char hex){
  byte ret;
  char s = "";
  int f;
  
  if(hex=='A' || hex=='a') f=10;
  else if(hex=='B' || hex=='b')f=11;
  else if(hex=='C' || hex=='c')f=12;
  else if(hex=='D' || hex=='d')f=13;
  else if(hex=='E' || hex=='e')f=14;
  else if(hex=='F' || hex=='f')f=15;
  else {s=hex; f = s - '0';}
  
  ret = (byte)f;
  return ret;
}

void onMSG(int bytes){
  Serial.println("MESSAGE RECEIVED");
}
