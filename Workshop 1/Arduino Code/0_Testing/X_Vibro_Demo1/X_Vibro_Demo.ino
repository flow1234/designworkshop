#include <Wire.h>

#define WIRE_ID 5 // vibro01

// Arduino Pro Mini PWM-Pins
int MOTOR_1 = 6;
unsigned long previousMillis = 0;
unsigned long currentMillis = 0;
long f = 0;
//int MOTOR_2 = 5;
//int MOTOR_3 = 9;

/* Arduino UNO PWM-Pins
int MOTOR_1 = 9;
int MOTOR_2 = 10;
int MOTOR_3 = 11;
*/
void setup() {
  Serial.begin(9600);
  Wire.begin(WIRE_ID);
  Wire.onReceive(receiveEvent); // Attach a function to trigger when something is received.
  
  // LOG
  Serial.print("Slave connected on ");
  Serial.println(WIRE_ID);

  // Init gadget
  // put your setup code here, to run once:
  //pinMode (MOTOR_1, OUTPUT);
  //pinMode (MOTOR_2, OUTPUT);
  //pinMode (MOTOR_3, OUTPUT);
}

void loop() {
  currentMillis = millis();
  /*
  Serial.print("previous Millis: ");
  Serial.println(previousMillis);
  Serial.print("current Millis: ");
  Serial.println(currentMillis);
  Serial.print("f: ");
  Serial.println(f);*/
      
  //f = currentMillis - previousMillis;
      //if (f>2000) previousMillis = currentMillis;

  /*
  currentMillis = millis();
  f = currentMillis - previousMillis;
  while (f<2000){
    currentMillis = millis();
    Serial.println("waiting ");
    Serial.println(f);
    f = currentMillis-previousMillis;
  }
  previousMillis=millis();*/
  //analogWrite( MOTOR_1 , 153 );  // 60% duty cycle ~ 100% of motor
  //delay(1000);
  //analogWrite( MOTOR_1 , 0 );  // 60% duty cycle ~ 100% of motor
  //delay(1000);
}

/**
 * On Recieve Function
 */
void receiveEvent(int bytes) {
  int ctr = 0;
  char p, d, s;
  
  while (0 < Wire.available()) { // loop through all bytes
    char c = Wire.read();
    if (ctr==2){p = c;}
    if (ctr==3){d = c;}
    if (ctr==4){s = c;}
    ctr++;
  }
  
  //LOG
  Serial.println("Call recieved from Master Device:");
  Serial.print("Pattern: ");
  Serial.println(p);
  Serial.print("Distance: ");
  Serial.println(d);
  Serial.print("Severity: ");
  Serial.println(s);
  
  triggerGadget(p,d,s);
}

/**
 * @param pattern  - pre-defined for each gadget
 * @param distance - Not required for test - '1': 50m , '2': 10m (time to travel 40m at 20kmh: 7s)
 * @param severity - Not required for test - '1': light, '2': medium, '3': severe
 */
void triggerGadget(char pattern, char distance, char severity) {
  Serial.println("TRIGGERGADGET()");
  
  switch (pattern){
    
    case '1': // constant vibrations at 100%

      Serial.println("PATTERN1");
      
      currentMillis = millis();
      f = currentMillis - previousMillis;
      while (f<2000){
        currentMillis = millis();
        Serial.println("waiting ");
        Serial.println(f);
        f = currentMillis-previousMillis;
      }
      previousMillis=millis();
      
    
      //analogWrite( MOTOR_1 , 153 );  // 60% duty cycle ~ 100% of motor
      //analogWrite( MOTOR_2 , 153 );  // 60% duty cycle ~ 100% of motor
      //analogWrite( MOTOR_3 , 153 );  // 60% duty cycle ~ 100% of motor
      delay(3000);                   // play for 1s

      Serial.println("AFTER DELAY");
    
      //analogWrite( MOTOR_1 , 0 );    // 0% duty cycle (off)
      //analogWrite( MOTOR_2 , 0 );    // 0% duty cycle (off)
      //analogWrite( MOTOR_3 , 0 );    // 0% duty cycle (off)
      
      break;
    default:
    
      break;
  }
}

