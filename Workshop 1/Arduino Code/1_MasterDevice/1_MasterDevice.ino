#include <Wire.h>

#define MAX_SLAVE_NODES 14

// Variables to store slave devices

byte slaveCount = 0;              // Count of active gadgets
boolean slaves[MAX_SLAVE_NODES];  // List to store active gadgets


// Variables to store incoming message from Android device

char inData[32];                  // Allocate some space for the string
char inChar=-1;                   // Where to store the character read
byte index = 0;                   // Index into array; where to store the character
boolean msgProcessed = false;     // Flag if the Android message was processed

/**
 * Arduino Init
 */
void setup() {
  Serial.begin(9600); // Start serial at 9600 baud
  Wire.begin();       // Start the I2C Bus as Master
  scanner();          // scan for available gadgets
}


/**
 * Arduino Loop
 */
void loop() {
  if(!msgProcessed){readAndroid();delay(2000);}
  delay(500);
}


/* 
 * =============== Android functions ===============
 */

void readAndroid(){
  if(Serial.available())
  {
   while(Serial.available() > 0) // Don't read unless
  {
   if(index < 32) // One less than the size of the array
   {
     inChar = Serial.read(); // Read a character
     inData[index] = inChar; // Store it
     index++;                // Increment where to write next
     inData[index] = '\0';   // Null terminate the string
   }
  }
  Serial.print("(Log) Received message: ");
  Serial.println(inData);
  index = 0;
  msgProcessed = true;
  processAndroid(inData);
  }
}

/**
 * 
 */
void processAndroid(char* This){
  if(This[0]=='0'){ // Broadcast
    // LOG
    Serial.println("(Log) Sending Broadcast");
    sendBroadcast(This);
  }
  else if(This[0]=='s'){ // Re-run Scanner
    // LOG
    Serial.println("(Log) Updating active Gadgets");
    scanner();
  }
  else if(This[0]=='1'){ // Specific gadget
    // LOG
    Serial.print("(Log) Sending notification to Gadget ");
    Serial.println(This[1]);
    notify(This);
  }
  msgProcessed=false;
}


/* 
 * =============== I2C functions ===============
 */

/** 
 * Scan for all available gadgets on the bus. 
 */
void scanner() {
  // Scan for available slave-nodes
  slaveCount = 0;
  // LOG
  Serial.println ("(Log) I2C scanner. Scanning ...");
  for (int i=0;i<MAX_SLAVE_NODES;i++)
  {
    slaves[i]=false;
  }
   for (byte i = 1; i < MAX_SLAVE_NODES; i++) // total number of slaves being tested
   {
     Wire.beginTransmission (i);
     if (Wire.endTransmission () == 0)
       {
       slaves[i]=true;
       Serial.print ("(Log) Found address: ");
       Serial.print (i, DEC);
       Serial.print (" (0x");
       Serial.print (i, HEX);
       Serial.println (")");
       slaveCount++;
       }
      delay (5);
   }
   Serial.println ("(Log) Done.");
   Serial.print ("(Log) Found ");
   Serial.print (slaveCount, DEC);
   Serial.println (" slave(s).");
  
   // Update Gadgets on Controller
   String updateGadgets = "";
   for (int i=0;i<MAX_SLAVE_NODES;i++)
    {
     if (slaves[i] != 0)
     {
       updateGadgets += String(i,DEC);
       updateGadgets += ", ";
     }
   }
   Serial.println("Z");
   Serial.println(updateGadgets.substring(0,updateGadgets.length()-2));
   Serial.println("Y");
}


/**
 * Notify all connected gadgets.
 */
void sendBroadcast(char* msg) {
  Serial.println("\nsendBroadcast()");
  for (int i=0;i<MAX_SLAVE_NODES;i++)
  {
    for (int j=3;j<strlen(msg);j+=2)
    {
      if (i==(int)hexCharToByte(msg[j]))
      {
        if (slaves[i] == 1)
        {
          char test[5];
          test[0]=msg[0];      //type
          test[1]=msg[j];   //target
          test[2]=msg[j+1]; //pattern
          test[3]=msg[1];   //distance
          test[4]=msg[2];   //severity
          notify(i,test);
        }
      }
    }
  }
}

/**
 * Notify a specific gadget.
 * 
 *  Gadget-IDs:
 *  0 - Master
 *  1 - leds01
 *  2 - leds02
 *  3 - leds03
 *  4 - leds04
 *  5 - vibro01
 *  6 - vibro02
 *  7 - vibro03
 *  8 - vibro04
 *  9 - ring01
 *  A - lumiTape01
 *  B - lumiTape02
 *  C - rgbStrip01
 *  D - rgbStrip02
 */
void notify(int id, char* msg) {
  // LOG
  Serial.print("(Log) Broadcast - Notify Slave ");
  Serial.print(id);
  Serial.print(": ");
  Serial.println(msg);
  Wire.beginTransmission((byte)id);
  Wire.write(msg);
  Wire.endTransmission();
}

void notify(char* msg){
  // LOG
  Serial.print("(Log) Notify Slave ");
  Serial.print(hexCharToByte(msg[1]));
  Serial.print(": ");
  Serial.println(msg);
  Wire.beginTransmission(hexCharToByte(msg[1]));
  Wire.write(msg);
  Wire.endTransmission();
}

byte hexCharToByte(char hex){
  byte ret;
  char s = "";
  int f;
  
  if(hex=='A' || hex=='a') f=10;
  else if(hex=='B' || hex=='b')f=11;
  else if(hex=='C' || hex=='c')f=12;
  else if(hex=='D' || hex=='d')f=13;
  else if(hex=='E' || hex=='e')f=14;
  else if(hex=='F' || hex=='f')f=15;
  else {s=hex; f = s - '0';}
  
  ret = (byte)f;
  return ret;
}
