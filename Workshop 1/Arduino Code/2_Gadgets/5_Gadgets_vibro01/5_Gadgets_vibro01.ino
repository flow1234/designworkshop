#include <Wire.h>

#define WIRE_ID 5 // vibro01

int MOTOR_1 = 3;
int MOTOR_2 = 5;
int MOTOR_3 = 9;

int routine = 0;

void setup() {
  Serial.begin(9600);
  Wire.begin(WIRE_ID);
  Wire.onReceive(receiveEvent); // Attach a function to trigger when something is received.
  
  // LOG
  Serial.print("Slave connected on ");
  Serial.println(WIRE_ID);

  pinMode (MOTOR_1, OUTPUT);
  pinMode (MOTOR_2, OUTPUT);
  pinMode (MOTOR_3, OUTPUT);
}

void loop() {

  if (routine==1){
    analogWrite( MOTOR_1 , 153 );  // 60% duty cycle ~ 100% of motor
    analogWrite( MOTOR_2 , 153 );  // 60% duty cycle ~ 100% of motor
    analogWrite( MOTOR_3 , 153 );  // 60% duty cycle ~ 100% of motor
    delay(3000);                   // play for 1s
    analogWrite( MOTOR_1 , 0 );    // 0% duty cycle (off)
    analogWrite( MOTOR_2 , 0 );    // 0% duty cycle (off)
    analogWrite( MOTOR_3 , 0 );    // 0% duty cycle (off)
    
    routine = 0;    
  }
  
  else if (routine==2){
    analogWrite( MOTOR_1 , 50 );  // 30% duty cycle ~ 33% of motor
    analogWrite( MOTOR_2 , 50 );  // 30% duty cycle ~ 33% of motor
    analogWrite( MOTOR_3 , 50 );  // 30% duty cycle ~ 33% of motor
    delay(3000);                   // play for 1s
    analogWrite( MOTOR_1 , 0 );    // 0% duty cycle (off)
    analogWrite( MOTOR_2 , 0 );    // 0% duty cycle (off)
    analogWrite( MOTOR_3 , 0 );    // 0% duty cycle (off)
    
    routine = 0;
  }
  
  else if (routine==3){
    for (int i=0;i<154;i++){
      analogWrite( MOTOR_1 , i );  // 60% duty cycle ~ 0%->100% of motor
      analogWrite( MOTOR_2 , i );  // 60% duty cycle ~ 0%->100% of motor
      analogWrite( MOTOR_3 , i );  // 60% duty cycle ~ 0%->100% of motor
      delay(20); // 3000ms/(153)iterations = 20 ~ 3s
    }
    analogWrite( MOTOR_1 , 0 );    // 0% duty cycle (off)
    analogWrite( MOTOR_2 , 0 );    // 0% duty cycle (off)
    analogWrite( MOTOR_3 , 0 );    // 0% duty cycle (off)
    
    routine = 0;
  }
  else if (routine==4){
    for (int i=0;i<15;i++){
      analogWrite( MOTOR_1 , 153 );  // 60% duty cycle ~ 100% of motor
      analogWrite( MOTOR_2 , 153 );  // 60% duty cycle ~ 100% of motor
      analogWrite( MOTOR_3 , 153 );  // 60% duty cycle ~ 100% of motor
      delay(100); // 15*100ms = 1.5s
      analogWrite( MOTOR_1 , 0 );    // 0% duty cycle (off)
      analogWrite( MOTOR_2 , 0 );    // 0% duty cycle (off)
      analogWrite( MOTOR_3 , 0 );    // 0% duty cycle (off)
      delay(100); // 15*100ms = 1.5s -> 2*1.5s = 3s
    }
    
    routine = 0;
  }
  else if (routine==5){
    for (int i=0;i<7;i++){
      analogWrite( MOTOR_1 , 153 );  // 60% duty cycle ~ 100% of motor
      analogWrite( MOTOR_2 , 153 );  // 60% duty cycle ~ 100% of motor
      analogWrite( MOTOR_3 , 153 );  // 60% duty cycle ~ 100% of motor
      delay(200); // 7*200ms = 1.4s
      analogWrite( MOTOR_1 , 0 );    // 0% duty cycle (off)
      analogWrite( MOTOR_2 , 0 );    // 0% duty cycle (off)
      analogWrite( MOTOR_3 , 0 );    // 0% duty cycle (off)
      delay(200); // 7*200ms = 1.4s -> 2*1.4s = 2.8s
    }
    
    routine = 0;
  } 
}

/**
 * On Recieve Function
 */
void receiveEvent(int bytes) {
  int ctr = 0;
  char p, d, s;
  
  while (0 < Wire.available()) { // loop through all bytes
    char c = Wire.read();
    if (ctr==2){p = c;}
    if (ctr==3){d = c;}
    if (ctr==4){s = c;}
    ctr++;
  }
  
  //LOG
  Serial.println("Call recieved from Master Device:");
  Serial.print("Pattern: ");
  Serial.println(p);
  Serial.print("Distance: ");
  Serial.println(d);
  Serial.print("Severity: ");
  Serial.println(s);
  
  triggerGadget(p,d,s);
}

/**
 * @param pattern  - pre-defined for each gadget
 * @param distance - Not required for test - '1': 50m , '2': 10m (time to travel 40m at 20kmh: 7s)
 * @param severity - Not required for test - '1': light, '2': medium, '3': severe
 */
void triggerGadget(char pattern, char distance, char severity) {
  
  switch (pattern){
    
    case '1': // constant vibrations at 100%
      
      routine = 1;
      break;
      

    case '2': // constant vibrations at 33%

      routine = 2;
      break;
      
      
    case '3': // ramping vibrations from 0% to 100%

      routine = 3;
      break;
      
      
    case '4': // cycling vibrations fast ( 14 ticks/s )

      routine = 4;
      break;
      
      
    case '5': // cycling vibrations slow ( 7 ticks/s )

      routine = 5;
      break;
      
    
    default:
    
      break;
  }
}

