#include <Wire.h>

#define WIRE_ID X // gadget_X

int routine = 0;


/**
 * Arduino Init
 */
void setup() {
  Serial.begin(9600);
  Wire.begin(WIRE_ID);
  Wire.onReceive(receiveEvent); // Attach a function to trigger when something is received.
  
  // LOG
  Serial.print("Slave connected on ");
  Serial.println(WIRE_ID);

  // Init gadget
}


/**
 * Arduino Loop
 */
void loop() {
  if (routine==1){
    //TODO
    routine = 0;    
  }  
  else if (routine==2){
    //TODO
    routine = 0;
  }
  else if (routine==3){
    //TODO   
    routine = 0;
  }
  else if (routine==4){
    //TODO
    routine = 0;
  }
  else if (routine==5){
    //TODO   
    routine = 0;
  }
  else if (routine==6){
    //TODO
    routine = 0;
  }
  else if (routine==7){
    //TODO
    routine = 0;
  }
}


/**
 * On Recieve Function
 */
void receiveEvent(int bytes) {
  int ctr = 0;
  char p, d, s;
  
  while (0 < Wire.available()) { // loop through all bytes
    char c = Wire.read();
    if (ctr==2){p = c;}
    if (ctr==3){d = c;}
    if (ctr==4){s = c;}
    ctr++;
  }
  
  //LOG
  Serial.println("Call recieved from Master Device:");
  Serial.print("Pattern: ");
  Serial.println(p);
  Serial.print("Distance: ");
  Serial.println(d);
  Serial.print("Severity: ");
  Serial.println(s);
  
  triggerGadget(p,d,s);
}


/**
 * ===================== Gadget functions ======================
 */
/**
 * @param pattern  - pre-defined for each gadget
 * @param distance - Not required for test - '1': 50m , '2': 10m (time to travel 40m at 20kmh: 7s)
 * @param severity - Not required for test - '1': light, '2': medium, '3': severe
 */
void triggerGadget(char pattern, char distance, char severity) {
  
  switch (pattern){
    
    case '1':

      routine = 1;
      break;

      
    case '2':
    
      routine = 2;
      break;

      
    case '3':

      routine = 3;
      break;

      
    case '4':
    
      routine = 4;
      break;


    case '5':
      
      routine = 5;
      break;


    case '6':

      routine = 6;
      break;


    case '7':

      routine = 7;
      break;

      
    default:
    
      break;
  }
}
