package com.hariharan.arduinousb;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

public class LoginFragment extends Fragment {

    View view;
    EditText ip_container;
    Button connect_btn;
    ProgressBar progressBar;
    AppData appData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login, container, false);

        appData = ((MainActivity)getActivity()).getAppData();
        connect_btn = (Button) view.findViewById(R.id.connect_btn);
        ip_container   = (EditText)view.findViewById(R.id.ip_container);
        progressBar = (ProgressBar)view.findViewById(R.id.connect_progress);

        connect_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                connect();
                ((MainActivity) getActivity()).loadFragment();
            }
        });

        return view;
    }

    public void connect(){
        String ip = ip_container.getText().toString();
        appData.setIP(ip);
        ((MainActivity)getActivity()).con = new IOConnection((MainActivity)getActivity());
    }

    public void showProgressBar(boolean b){
        if (!b) {
            progressBar.setVisibility(View.GONE);
        }
        else {
            progressBar.setVisibility(View.VISIBLE);
        }
    }
}