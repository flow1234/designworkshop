package com.hariharan.arduinousb;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Florian Wirth on 26/10/2017.
 */

public class ArduinoAdapter {

    private AppData appData;
    private String activeGadgets;
    private MainActivity mainActivity;
    private Map<String, Integer> myMap = new HashMap<String, Integer>();


    public ArduinoAdapter(MainActivity activity){
        mainActivity = activity;
        appData = mainActivity.getAppData();
        createDictionary();
    }

    public void requestUpdate() {
        String message = "s";
        if(appData.isSerialConnected())MainActivity.serialPort.write(message.getBytes());
        else mainActivity.log_buffer.append("\n(Error) Couldn't request update. Connection is closed.");
    }

    public void updateGadgets(String gadgets) {
        gadgets = gadgets.trim();
        String gadgets1[] = gadgets.split(" ");
        activeGadgets = "[";
        for (int i=0;i<gadgets1.length;i++)
        {
            for (Map.Entry<String, Integer> e : myMap.entrySet()) {
                String key = e.getKey();
                Integer value = e.getValue();
                if (value==Integer.parseInt(gadgets1[i])){
                    activeGadgets += "{\"gadget\":\""+key+"\"},";
                }
            }
        }
        activeGadgets = activeGadgets.substring(0,activeGadgets.length()-1);
        activeGadgets += "]";
    }

    public String getGadgets() {
        return activeGadgets;
    }

    public void notify(Object args) {

        try {
            Gson gson = new Gson();
            String data = gson.toJson(args);          //convert response to string
            data = data.replaceAll("\\\\","");        //replace all backslashes
            data = data.substring(2,data.length()-2); //remove first and last two characters

            if (String.valueOf(data.charAt(0)).equals("[")) {
                // It's an array
                JSONArray jsonArray = new JSONArray(data);
                String notify = "";
                boolean firstElement = true;
                boolean notifySmartphonedisplay = false;
                boolean notifySmartphonespeakers = false;
                int smartphoneAudioPattern = 1;

                for (int i=0;i<jsonArray.length();i++){
                    if (!Boolean.parseBoolean(jsonArray.getJSONObject(i).getString("disabled")))
                    {
                        if (jsonArray.getJSONObject(i).getString("gadget").equals("phone01")){
                            if (appData.isLogActive()){
                                appData.setLogActive(false);
                                mainActivity.loadFragment();
                            }
                            notifySmartphonedisplay = true;
                        }
                        if (jsonArray.getJSONObject(i).getString("gadget").equals("phone02")){
                            if (appData.isLogActive()){
                                appData.setLogActive(false);
                                mainActivity.loadFragment();
                            }
                            notifySmartphonespeakers = true;
                            smartphoneAudioPattern = Integer.parseInt(jsonArray.getJSONObject(i).getString("pattern"));
                        }

                        String type = "0";

                        String pattern = jsonArray.getJSONObject(i).getString("pattern");
                        String distance = jsonArray.getJSONObject(i).getString("distance");
                        String severity = jsonArray.getJSONObject(i).getString("severity");

                        try {
                            String id = myMap.get(jsonArray.getJSONObject(i).getString("gadget")).toString();
                            switch(id) {
                                case "10":
                                    id = "A"; break;
                                case "11":
                                    id = "B"; break;
                                case "12":
                                    id = "C"; break;
                                case "13":
                                    id = "D"; break;
                                case "14":
                                    id = "E"; break;
                                case "15":
                                    id = "F"; break;
                                default:
                                    break;
                            }

                            if (firstElement){
                                notify+=type+distance+severity;
                                firstElement = false;
                            }
                            notify += id+pattern;

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }
                System.out.println(notify);
                if (appData.isSerialConnected()) MainActivity.serialPort.write(notify.getBytes());
                else mainActivity.log_buffer.append("\n(Error) Couldn't notify gadgets. Connection is closed.");

                MainFragment myFrag = (MainFragment)mainActivity.getFragmentManager().findFragmentByTag("main");
                if (myFrag != null){
                    if (notifySmartphonedisplay) myFrag.blink();
                    if (notifySmartphonespeakers) myFrag.playAudio(smartphoneAudioPattern);
                }
            }
            else {
                // It's an object
                JSONObject jsonObject = new JSONObject(data);
                if (!Boolean.parseBoolean(jsonObject.getString("disabled")))
                {
                    if (jsonObject.getString("gadget").equals("phone01")){
                        MainFragment myFrag = (MainFragment)mainActivity.getFragmentManager().findFragmentByTag("main");
                        if (myFrag!=null) myFrag.blink();
                    }
                    if (jsonObject.getString("gadget").equals("phone02")){
                        MainFragment myFrag = (MainFragment)mainActivity.getFragmentManager().findFragmentByTag("main");
                        if (myFrag!=null) myFrag.playAudio(Integer.parseInt(jsonObject.getString("pattern")));
                    }

                    String notify = "";
                    String type = "1";

                    String pattern = jsonObject.getString("pattern");
                    String distance = jsonObject.getString("distance");
                    String severity = jsonObject.getString("severity");

                    try {
                        String id = myMap.get(jsonObject.getString("gadget")).toString();
                        switch(id) {
                            case "10":
                                id = "A"; break;
                            case "11":
                                id = "B"; break;
                            case "12":
                                id = "C"; break;
                            case "13":
                                id = "D"; break;
                            case "14":
                                id = "E"; break;
                            case "15":
                                id = "F"; break;
                            default:
                                break;
                        }

                        notify += type+id+pattern+distance+severity;
                        if(appData.isSerialConnected())MainActivity.serialPort.write(notify.getBytes());
                        else mainActivity.log_buffer.append("\n(Error) Couldn't notify gadget. Connection is closed.");

                        System.out.println(notify);

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void createDictionary(){
        /*myMap.put("leds01",1);
        myMap.put("leds02",2);
        myMap.put("leds03",3);
        myMap.put("leds04",4);
        myMap.put("vibro01",5);
        myMap.put("vibro02",6);
        myMap.put("vibro03",7);
        myMap.put("vibro04",8);
        myMap.put("ring01",9);
        myMap.put("lumiTape01",10);
        myMap.put("lumiTape02",11);
        myMap.put("rgbStrip01",12);
        myMap.put("rgbStrip02",13);*/
        myMap.put("rgbStrip01",1);
        myMap.put("rgbStrip02",2);
        myMap.put("rgbStrip03",3);
        myMap.put("rgbStrip04",4);
        myMap.put("vibro01",5);
        myMap.put("vibro02",6);
        myMap.put("vibro03",7);
        myMap.put("vibro04",8);
        myMap.put("ring01",9);
    }
}
