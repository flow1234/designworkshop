package com.hariharan.arduinousb;

import android.os.Handler;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

/**
 * Created by Florian Wirth on 2/11/2017.
 */

public class IOConnection {

    private Socket socket;
    private MainActivity mainActivity;

    private AppData appData;

    public IOConnection(MainActivity activity){
        mainActivity = activity;
        appData = mainActivity.getAppData();
        init();
        connect();
    }

    private void init(){
        try {
            socket = IO.socket("http://"+appData.getIP()+":3000");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void connect(){
        final Handler handler = new Handler();
        LoginFragment myFrag = (LoginFragment)mainActivity.getFragmentManager().findFragmentByTag("login");
        if (myFrag!=null&&myFrag.isVisible()) myFrag.showProgressBar(true);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!socket.connected()){
                    mainActivity.loadFragment();
                    LoginFragment myFrag = (LoginFragment)mainActivity.getFragmentManager().findFragmentByTag("login");
                    if (myFrag!=null&&myFrag.isVisible()) myFrag.showProgressBar(false);
                    Toast.makeText(mainActivity.getApplicationContext(),"IOConnection failed", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(mainActivity.getApplicationContext(),"IOConnection successful", Toast.LENGTH_SHORT).show();
                    appData.setWifiConnected(true);
                    mainActivity.log_buffer.append("\n(LOG) Wifi Connection established\n");
                    LoginFragment myFrag = (LoginFragment)mainActivity.getFragmentManager().findFragmentByTag("login");
                    if (myFrag!=null&&myFrag.isVisible()) myFrag.showProgressBar(false);
                    mainActivity.loadFragment();
                }
            }
        }, 1000);

        getSocket().connect();
    }

    public Socket getSocket() {
        return socket;
    }

    public void addHandlers(ArduinoAdapter arduinoAdapter){

        final ArduinoAdapter arduino = arduinoAdapter;

        socket.on("BC_update", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        arduino.notify(args);
                    }
                });
            }
        });

        socket.on("BC_request", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        arduino.requestUpdate();
                    }
                });
            }
        });
    }
}
