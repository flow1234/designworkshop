package com.hariharan.arduinousb;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

public class MainFragment extends Fragment {

    View view;
    AppData appData;

    View logoView, gadgetView;

    AudioGadget audioGadget;
    boolean audioPlaying;

    ImageView img;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_main, container, false);

        appData = ((MainActivity)getActivity()).getAppData();

        logoView = (View)view.findViewById(R.id.logoView);
        gadgetView = (View)view.findViewById(R.id.gadgetView);
        audioGadget = new AudioGadget(getActivity().getApplicationContext());

        gadgetView.setVisibility(View.GONE);

        img = (ImageView)view.findViewById(R.id.imageView1);

        audioPlaying = false;

        return view;
    }

    public void blink(){
        gadgetView.setVisibility(View.VISIBLE);
        logoView.setVisibility(View.GONE);

        final Animation animation = new AlphaAnimation(1, 0);
        animation.setAnimationListener(new Animation.AnimationListener()
        {

            @Override
            public void onAnimationStart(Animation animation) { }

            @Override
            public void onAnimationRepeat(Animation animation) { }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                gadgetView.setVisibility(View.GONE);
                logoView.setVisibility(View.VISIBLE);
                if (audioPlaying){
                    audioGadget.stop();
                    audioPlaying = false;
                }
            }
        });
        animation.setDuration(700);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(5);
        animation.setRepeatMode(Animation.REVERSE);
        img.startAnimation(animation);
    }

    public void playAudio(int file){
        audioPlaying = true;
        audioGadget.play(file);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (audioPlaying) {
                    audioGadget.stop();
                    audioPlaying = false;
                }
            }
        }, 4000);//millisec.
    }
}