package com.hariharan.arduinousb;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

public class DebugFragment extends Fragment {

    View view;
    TextView textView;
    EditText editText;
    Button start,stop,send,clear;

    AppData appData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_debug, container, false);

        appData = ((MainActivity)getActivity()).getAppData();

        textView = (TextView) view.findViewById(R.id.textView);
        editText = (EditText) view.findViewById(R.id.editText);

        start = (Button) view.findViewById(R.id.buttonStart);
        stop = (Button) view.findViewById(R.id.buttonStop);
        send = (Button) view.findViewById(R.id.buttonSend);
        clear = (Button) view.findViewById(R.id.buttonClear);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).onClickStart();
                updateUI();
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).onClickStop();
                updateUI();
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).onClickSend(editText.getText().toString());
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).log_buffer.setText("");
                updateUI();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    public void setUiEnabled(boolean bool) {
        start.setEnabled(!bool);
        send.setEnabled(bool);
        stop.setEnabled(bool);
    }

    public void updateUI(){
        textView.setText(appData.getLogText());
        setUiEnabled(appData.isUiEnabled());
        final ScrollView scrollview = ((ScrollView) view.findViewById(R.id.textAreaScroller));
        scrollview.post(new Runnable() {
            @Override
            public void run() {
                scrollview.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }
}