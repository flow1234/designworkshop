package com.hariharan.arduinousb;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.hariharan.arduinousb.R.string.debugActive;

public class MainActivity extends FragmentActivity{

    public final String ACTION_USB_PERMISSION = "com.hariharan.arduinousb.USB_PERMISSION";

    private AppData appData;

    private UsbManager usbManager;
    private UsbDevice device;
    public static UsbSerialDevice serialPort;
    private UsbDeviceConnection connection;

    TextView log_buffer;

    private ArduinoAdapter arduino;
    IOConnection con;

    private boolean recordflag = false;
    private String activeGadgets = "";

    private Button debug_button;
    private MainFragment mainFragment;
    private DebugFragment debugFragment;
    private LoginFragment loginFragment;

    UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() { //Defining a Callback which triggers whenever data is read.
        @Override
        public void onReceivedData(byte[] arg0) {
            String data = null;
            try {
                data = new String(arg0, "UTF-8");
                data.concat("/n");
                if (!recordflag) {
                    if (!data.contains("Z"))tvAppend(data);
                }
                if (data.contains("Z")){
                    recordflag=true;
                }
                if (recordflag) {
                    activeGadgets+=data;
                }
                if (data.contains("Y")){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateController();
                            activeGadgets ="";
                            recordflag=false;
                        }
                    });
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    };


    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() { //Broadcast Receiver to automatically start and stop the Serial connection.
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_USB_PERMISSION)) {
                boolean granted = intent.getExtras().getBoolean(UsbManager.EXTRA_PERMISSION_GRANTED);
                if (granted) {
                    connection = usbManager.openDevice(device);
                    serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection);
                    if (serialPort != null) {
                        if (serialPort.open()) { //Set Serial Connection Parameters.
                            appData.setUiEnabled(true);
                            serialPort.setBaudRate(9600);
                            serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
                            serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
                            serialPort.setParity(UsbSerialInterface.PARITY_NONE);
                            serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
                            serialPort.read(mCallback);
                            log_buffer.append("\n(Log) Serial Connection Opened!\n");
                            appData.setSerialConnected(true);
                            DebugFragment myFrag = (DebugFragment)getFragmentManager().findFragmentByTag("debug");
                            if (myFrag!=null&&myFrag.isVisible()) myFrag.updateUI();
                        } else {
                            Log.d("SERIAL", "PORT NOT OPEN");
                        }
                    } else {
                        Log.d("SERIAL", "PORT IS NULL");
                    }
                } else {
                    Log.d("SERIAL", "PERM NOT GRANTED");
                }
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
                appData.setUiEnabled(true);
                appData.setSerialConnected(true);
                DebugFragment myFrag = (DebugFragment)getFragmentManager().findFragmentByTag("debug");
                if (myFrag!=null&&myFrag.isVisible()) myFrag.updateUI();
                onClickStart();
            } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
                appData.setUiEnabled(false);
                appData.setSerialConnected(false);
                DebugFragment myFrag = (DebugFragment)getFragmentManager().findFragmentByTag("debug");
                if (myFrag!=null&&myFrag.isVisible()) myFrag.updateUI();
                onClickStop();
            }
        };
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appData = new AppData();
        appData.setIP(getString(R.string.ip_hint));

        log_buffer = new TextView(getApplicationContext());

        debug_button = (Button)findViewById(R.id.debug_button);
        mainFragment = new MainFragment();
        debugFragment = new DebugFragment();
        loginFragment = new LoginFragment();

        appData.setLogActive(true);

        loadFragment();

        con = new IOConnection(this);
        arduino = new ArduinoAdapter(this);
        con.addHandlers(arduino);

        usbManager = (UsbManager) getSystemService(this.USB_SERVICE);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(broadcastReceiver, filter);

        debug_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment();
            }
        });

        log_buffer.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (appData.isLogActive()){
                    appData.setLogText(log_buffer.getText().toString());
                    DebugFragment myFrag = (DebugFragment)getFragmentManager().findFragmentByTag("debug");
                    if (myFrag!=null&&myFrag.isVisible()) myFrag.updateUI();
                }
            }
        });
    }

    public void onClickStart() {
        HashMap<String, UsbDevice> usbDevices = usbManager.getDeviceList();
        if (!usbDevices.isEmpty()) {
            boolean keep = true;
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {
                device = entry.getValue();
                int deviceVID = device.getVendorId();
                if (deviceVID == 0x2341)//Arduino Vendor ID
                {
                    PendingIntent pi = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
                    usbManager.requestPermission(device, pi);
                    keep = false;
                } else {
                    connection = null;
                    device = null;
                }
                if (!keep)
                    break;
            }
        }
    }

    public void onClickSend(String string) {
        if(appData.isSerialConnected())serialPort.write(string.getBytes());
        else log_buffer.append("\n(Error) Couldn't send message. Connection is closed.");
        log_buffer.append("\nData Sent: "+string+"\n");
    }

    public void onClickStop() {
        serialPort.close();
        appData.setSerialConnected(false);
        appData.setUiEnabled(false);
        DebugFragment myFrag = (DebugFragment)getFragmentManager().findFragmentByTag("debug");
        if (myFrag!=null&&myFrag.isVisible()) myFrag.updateUI();
        log_buffer.append("\n(Log) Serial Connection Closed!\n");
    }

    private void tvAppend(CharSequence text) {
        final CharSequence ftext = text;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                log_buffer.append(ftext);
            }
        });
    }

    private void updateController(){
        activeGadgets += " ";
        appData.setGadgets(activeGadgets.replaceAll("[^0-9]+", " "));
        arduino.updateGadgets(appData.getGadgets());
        con.getSocket().emit("CB_update",arduino.getGadgets());
    }

    public void loadFragment() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        if (appData.isWifiConnected()) {
            if (appData.isLogActive()){
                fragmentTransaction.replace(R.id.frameLayout, mainFragment, "main").commit();
                debug_button.setVisibility(View.VISIBLE);
                debug_button.setText(R.string.debugInactive);
                appData.setLogActive(false);
            }
            else {
                fragmentTransaction.replace(R.id.frameLayout, debugFragment, "debug").commit();
                debug_button.setVisibility(View.VISIBLE);
                debug_button.setText(debugActive);
                appData.setLogActive(true);
            }
        }
        else {
            debug_button.setVisibility(View.GONE);
            fragmentTransaction.replace(R.id.frameLayout, loginFragment, "login").commit();
        }
    }

    public AppData getAppData(){
        return appData;
    }
}