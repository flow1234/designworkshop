#include <Wire.h>
#include <Adafruit_NeoPixel.h>

#define STRIP_LEFT 3
#define STRIP_RIGHT 5
#define RING_CENTER 6
#define VIBRO_LEFT 9
#define VIBRO_RIGHT 10

// Variables to store incoming message from Android device

char inData[32];                  // Allocate some space for the string
char inChar = -1;                 // Where to store the character read
byte index = 0;                   // Index into array; where to store the character
boolean msgProcessed = false;     // Flag if the Android message was processed


// Variables to store current setup
char video;
char hazard;
char sev;
char dir;
char nCars;
char nCyclists;
char nPedestrians;
char nNature;
char nTerrain;
char nBehavior;
char hazardLvl;
char hLvlDisplay;
char hLvlColor;

char ledRing_1;
char ledRing_2;
char ledRing_3;
char ledRing_4;

char ledHandles_1;
char ledHandles_2;
char ledHandles_3;
char ledHandles_4;

char vibroHandles_1;
char vibroHandles_2;
char vibroHandles_3;
char vibroHandles_4;

char vibroHelmet_1;
char vibroHelmet_2;
char vibroHelmet_3;
char vibroHelmet_4;


// Gadgets
boolean setupComplete = false;
boolean gadgetsEnabled = true;
boolean newHazard = false;
boolean updateHazardLevel = false;

boolean ringEnabled = false;
boolean stripsEnabled = false;
boolean vibroHandlesEnabled = false;

boolean testCase = false;
boolean testRing = false;
boolean testStrips = false;
boolean testVibroHandles = false;



Adafruit_NeoPixel ring = Adafruit_NeoPixel(16, RING_CENTER, NEO_RGBW + NEO_KHZ800);
Adafruit_NeoPixel strip_left = Adafruit_NeoPixel(5, STRIP_LEFT, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip_right = Adafruit_NeoPixel(5, STRIP_RIGHT, NEO_GRB + NEO_KHZ800);


/*
   ============================== Arduino init functions =============================
*/
void setup() {
  Serial.begin(9600); // Start serial at 9600 baud
  Wire.begin();       // Start the I2C Bus as Master

  // Init gadget RING
  ring.begin();
  ring.setBrightness(30); //adjust brightness here
  ring.show(); // Initialize all pixels to 'off'

  // Init gadget STRIP_RIGHT
  strip_right.begin();
  strip_right.show(); // Initialize all pixels to 'off'

  // Init gadget STRIP_LEFT
  strip_left.begin();
  strip_left.show(); // Initialize all pixels to 'off'
  
  scanner();
}

void loop() {
  // ==================================================================================
  // GET ANDROID MESSAGE
  // ==================================================================================
  if (!msgProcessed) {
    readAndroid();

    // ================================================================================
    // TEST SPECIFIC GADGET
    // ================================================================================
    if (testCase){
      uint32_t ring_color;
      uint32_t strip_color;
      if (testRing){
        switch (ledRing_2) { // ring color
          case '1': ring_color = ring.Color(0, 255, 0); break; // red
          case '2': ring_color = ring.Color(255, 255, 0); break; // yellow
          case '3': ring_color = ring.Color(255, 0, 0); break; // green
          case '4': ring_color = ring.Color(255, 0, 255); break; // blue
          case '5': ring_color = ring.Color(0, 0, 255); break; // purple
          default: break;
        }
        if (ledRing_3 == '1') {
          colorWipeRing(ring_color, 0);
          delay(2000);
          colorWipeRing(ring.Color(0,0,0),0);
        }
        else if (ledRing_3 == '2') {
          for (int i=0;i<4;i++){
            colorWipeRing(ring_color, 0);
            delay(250);
            colorWipeRing(ring.Color(0,0,0),0);
            delay(250);
          }
        }
        else if (ledRing_3 == '3') {
          for (int i=0;i<4;i++){
            colorWipeRing(ring_color, 40);
            delay(210);
            colorWipeRing(ring.Color(0,0,0),0);
            delay(250);
          }
        }        
      }
      if (testStrips){
        switch (ledHandles_2) { // ring color
          case '1': strip_color = strip_right.Color(255, 0, 0); break; // red
          case '2': strip_color = strip_right.Color(255, 255, 0); break; // yellow
          case '3': strip_color = strip_right.Color(0, 255, 0); break; // green
          case '4': strip_color = strip_right.Color(0, 0, 255); break; // blue
          case '5': strip_color = strip_right.Color(255, 0, 255); break; // purple
          default: break;
        }
        if (ledHandles_3 == '1') {
          colorWipeStripLeft(strip_color, 0); 
          colorWipeStripRight(strip_color, 0);
          delay(2000);
          colorWipeStripLeft(ring.Color(0, 0, 0), 0); 
          colorWipeStripRight(ring.Color(0, 0, 0), 0);
        }
        else if (ledHandles_3 == '2') {
          for (int i=0;i<4;i++){
            colorWipeStripLeft(strip_color, 0); 
            colorWipeStripRight(strip_color, 0);
            delay(250);
            colorWipeStripLeft(ring.Color(0, 0, 0), 0); 
            colorWipeStripRight(ring.Color(0, 0, 0), 0);
            delay(250);
          }
        }
        else if (ledHandles_3 == '3') {
          for (int i=0;i<4;i++){
            colorWipeStripLeft(strip_color, 40); 
            colorWipeStripRight(strip_color, 40);
            delay(210);
            colorWipeStripLeft(ring.Color(0, 0, 0), 0); 
            colorWipeStripRight(ring.Color(0, 0, 0), 0);
            delay(250);
          }
        }
      }
      if (testVibroHandles){
        //TODO
      }
      testCase = false;
    }

    
    // ================================================================================
    // UPDATE HAZARD LEVEL
    // ================================================================================
    if (updateHazardLevel) {
      if (hazardLvl == '1') {
        if (hLvlDisplay == '1') { // ring selected
          switch (hLvlColor) {
            case '1': // red
              colorWipeRing(ring.Color(0, 255, 0), 0);
              break;
            case '2': // yellow
              colorWipeRing(ring.Color(255, 255, 0), 0);
              break;
            case '3': // green
              colorWipeRing(ring.Color(255, 0, 0), 0);
              break;
            case '4': // off
              colorWipeRing(ring.Color(0, 0, 0), 0);
              break;
            default:
              break;
          }
        }
        else if (hLvlDisplay == '2') { // strips selected
          switch (hLvlColor) {
            case '1': // red
              colorWipeStripRight(strip_right.Color(255, 0, 0), 0);
              colorWipeStripLeft(strip_left.Color(255, 0, 0), 0);
              break;
            case '2': // yellow
              colorWipeStripRight(strip_right.Color(255, 255, 0), 0);
              colorWipeStripLeft(strip_left.Color(255, 255, 0), 0);
              break;
            case '3': // green
              colorWipeStripRight(strip_right.Color(0, 255, 0), 0);
              colorWipeStripLeft(strip_left.Color(0, 255, 0), 0);
              break;
            case '4': // off
              colorWipeStripRight(strip_right.Color(0, 0, 0), 0);
              colorWipeStripLeft(strip_left.Color(0, 0, 0), 0);
              break;
            default:
              break;
          }
        }
      }
      updateHazardLevel = false;
    }


    // ================================================================================
    // TRIGGER GADGETS
    // ================================================================================

    if (newHazard) {
      uint32_t ring_color;
      uint32_t strip_color;
      int hazardDuration;

      if (ringEnabled) {
        switch (ledRing_2) { // ring color
          case '1': ring_color = ring.Color(0, 255, 0); break; // red
          case '2': ring_color = ring.Color(255, 255, 0); break; // yellow
          case '3': ring_color = ring.Color(255, 0, 0); break; // green
          case '4': ring_color = ring.Color(255, 0, 255); break; // blue
          case '5': ring_color = ring.Color(0, 0, 255); break; // purple
          default: break;
        }
      }
      if (stripsEnabled) {
        switch (ledHandles_2) { // ring color
          case '1': strip_color = strip_right.Color(255, 0, 0); break; // red
          case '2': strip_color = strip_right.Color(255, 255, 0); break; // yellow
          case '3': strip_color = strip_right.Color(0, 255, 0); break; // green
          case '4': strip_color = strip_right.Color(0, 0, 255); break; // blue
          case '5': strip_color = strip_right.Color(255, 0, 255); break; // purple
          default: break;
        }
      }


      // ================================================================================
      // VIDEO 1 - HAZARDS:
      // 1: Pedestrian on the Right
      // 2: 
      // ================================================================================
      if (video == '1' && hazard == '1') {
        hazardDuration = 2; // 2 seconds
        if (nPedestrians == '1') setupComplete = true;
      }    
      else if (video == '1' && hazard == '2') {
        hazardDuration = 5; // 4 seconds
        if (nCyclists == '1' || nTerrain == '1') setupComplete = true;
      }
      else if (video == '1' && hazard == '3') {
        hazardDuration = 2; // 4 seconds
        if (nPedestrians == '1') setupComplete = true;
      }
      else if (video == '1' && hazard == '4') {
        hazardDuration = 1; // 4 seconds
        if (nPedestrians == '1') setupComplete = true;
      }
      else if (video == '1' && hazard == '5') {
        hazardDuration = 1; // 4 seconds
        if (nPedestrians == '1') setupComplete = true;
      }
      else if (video == '1' && hazard == '6') {
        hazardDuration = 5; // 4 seconds
        if (nPedestrians == '1') setupComplete = true;
      }
      else if (video == '1' && hazard == '7') {
        hazardDuration = 1; // 4 seconds
        if (nNature == '1') setupComplete = true;
      }
      else if (video == '1' && hazard == '8') {
        hazardDuration = 3; // 4 seconds
        if (nPedestrians == '1') setupComplete = true;
      }
      else if (video == '1' && hazard == '9') {
        hazardDuration = 2; // 4 seconds
        if (nCars == '1') setupComplete = true;
      }
      else if (video == '1' && hazard == 'A') {
        hazardDuration = 8; // 4 seconds
        if (nCars == '1') setupComplete = true;
      }




      // ================================================================================
      // TRIGGER HAZARD
      // ================================================================================
      if (setupComplete) {
    
       for (int i = 0; i < hazardDuration; i++) {

          if (ringEnabled) {

            if (ledRing_4 == '1' && ledRing_3 == '1') colorWipeRing(ring_color, 0);
            else if (ledRing_4 == '1' && ledRing_3 == '2') colorWipeRing(ring_color, 0);
            else if (ledRing_4 == '1' && ledRing_3 == '3') colorWipeRing(ring_color, 40);
            else if (ledRing_4 == '0' && ledRing_3 == '1') colorWipeRing(ring_color, 0);
            else if (ledRing_4 == '0' && ledRing_3 == '2') colorWipeRing(ring_color, 0);
            else if (ledRing_4 == '0' && ledRing_3 == '3') colorWipeRing(ring_color, 40);
          }

          if (stripsEnabled) {

            if (ledHandles_4 == '1' && ledHandles_3 == '1') {
              switch (dir) {
                case '1': colorWipeStripLeft(strip_color, 0); colorWipeStripRight(strip_color, 0); break; //front
                case '2': colorWipeStripRight(strip_color, 0); break; //right
                case '3': colorWipeStripLeft(strip_color, 0); colorWipeStripRight(strip_color, 0); break; //back
                case '4': colorWipeStripLeft(strip_color, 0); break; //left
                default: break;
              }
            }
            else if (ledHandles_4 == '1' && ledHandles_3 == '2') {
              switch (dir) {
                case '1': colorWipeStripLeft(strip_color, 0); colorWipeStripRight(strip_color, 0); break; //front
                case '2': colorWipeStripRight(strip_color, 0); break; //right
                case '3': colorWipeStripLeft(strip_color, 0); colorWipeStripRight(strip_color, 0); break; //back
                case '4': colorWipeStripLeft(strip_color, 0); break; //left
                default: break;
              }
            }
            else if (ledHandles_4 == '1' && ledHandles_3 == '3') {
              switch (dir) {
                case '1': colorWipeStripLeft(strip_color, 40); colorWipeStripRight(strip_color, 40); break; //front
                case '2': colorWipeStripRight(strip_color, 40); break; //right
                case '3': colorWipeStripLeft(strip_color, 40); colorWipeStripRight(strip_color, 40); break; //back
                case '4': colorWipeStripLeft(strip_color, 40); break; //left
                default: break;
              }
            }
            else if (ledHandles_4 == '0' && ledHandles_3 == '1') {
              colorWipeStripLeft(strip_color, 0); 
              colorWipeStripRight(strip_color, 0);
            }
            else if (ledHandles_4 == '0' && ledHandles_3 == '2') {
              colorWipeStripLeft(strip_color, 0); 
              colorWipeStripRight(strip_color, 0);
            }
            else if (ledHandles_4 == '0' && ledHandles_3 == '3') {
              colorWipeStripLeft(strip_color, 40); 
              colorWipeStripRight(strip_color, 40);
            }
          }

          delay(500);

          if (ringEnabled) {
            if (ledRing_4 == '1' && ledRing_3 == '1') colorWipeRing(ring_color, 0);
            else if (ledRing_4 == '1' && ledRing_3 == '2') colorWipeRing(ring.Color(0, 0, 0), 0);
            else if (ledRing_4 == '1' && ledRing_3 == '3') {colorWipeRing(ring.Color(0, 0, 0), 0); colorWipeRing(ring_color, 40);}
            else if (ledRing_4 == '0' && ledRing_3 == '1') colorWipeRing(ring_color, 0);
            else if (ledRing_4 == '0' && ledRing_3 == '2') colorWipeRing(ring.Color(0, 0, 0), 0);
            else if (ledRing_4 == '0' && ledRing_3 == '3') {colorWipeRing(ring.Color(0, 0, 0), 0); colorWipeRing(ring_color, 40);}
          }

          if (stripsEnabled) {
            if (ledHandles_4 == '1' && ledHandles_3 == '1') {
              switch (dir) {
                case '1': colorWipeStripLeft(strip_color, 0); colorWipeStripRight(strip_color, 0); break; //front
                case '2': colorWipeStripRight(strip_color, 0); break; //right
                case '3': colorWipeStripLeft(strip_color, 0); colorWipeStripRight(strip_color, 0); break; //back
                case '4': colorWipeStripLeft(strip_color, 0); break; //left
                default: break;
              }
            }
            else if (ledHandles_4 == '1' && ledHandles_3 == '2') {
              switch (dir) {
                case '1': colorWipeStripLeft(ring.Color(0, 0, 0), 0); colorWipeStripRight(ring.Color(0, 0, 0), 0); break; //front
                case '2': colorWipeStripRight(ring.Color(0, 0, 0), 0); break; //right
                case '3': colorWipeStripLeft(ring.Color(0, 0, 0), 0); colorWipeStripRight(ring.Color(0, 0, 0), 0); break; //back
                case '4': colorWipeStripLeft(ring.Color(0, 0, 0), 0); break; //left
                default: break;
              }
            }
            else if (ledHandles_4 == '1' && ledHandles_3 == '3') {
              switch (dir) {
                case '1': colorWipeStripLeft(ring.Color(0, 0, 0), 0); colorWipeStripRight(ring.Color(0, 0, 0), 0); colorWipeStripLeft(strip_color, 40); colorWipeStripRight(strip_color, 40); break; //front
                case '2': colorWipeStripRight(ring.Color(0, 0, 0), 0); colorWipeStripRight(strip_color, 40); break; //right
                case '3': colorWipeStripLeft(ring.Color(0, 0, 0), 0); colorWipeStripRight(ring.Color(0, 0, 0), 0); colorWipeStripLeft(strip_color, 40); colorWipeStripRight(strip_color, 40); break; //back
                case '4': colorWipeStripLeft(ring.Color(0, 0, 0), 0); colorWipeStripLeft(strip_color, 40); break; //left
                default: break;
              }
            }
            else if (ledHandles_4 == '0' && ledHandles_3 == '1') {
              colorWipeStripLeft(strip_color, 0); 
              colorWipeStripRight(strip_color, 0);
            }
            else if (ledHandles_4 == '0' && ledHandles_3 == '2') {
              colorWipeStripLeft(ring.Color(0, 0, 0), 0); 
              colorWipeStripRight(ring.Color(0, 0, 0), 0);
            }
            else if (ledHandles_4 == '0' && ledHandles_3 == '3') {
              colorWipeStripLeft(ring.Color(0, 0, 0), 0); 
              colorWipeStripRight(ring.Color(0, 0, 0), 0); 
              colorWipeStripLeft(strip_color, 40); 
              colorWipeStripRight(strip_color, 40);
             }
          }
          delay(500);
        }
        
        // ================================================================================
        // TURN OFF DISABLED GADGETS FOR HAZARD LEVEL DISPLAY
        // ================================================================================
        
        if (hazardLvl == '0') { // if disabled: turn off both gadgets
          colorWipeRing(ring.Color(0, 0, 0), 0);
          colorWipeStripLeft(strip_left.Color(0, 0, 0), 0);
          colorWipeStripRight(strip_right.Color(0, 0, 0), 0);
        }
        else if (hazardLvl == '1' && hLvlDisplay == '1') { // if ring enabled: turn off strips
          colorWipeStripLeft(strip_left.Color(0, 0, 0), 0);
          colorWipeStripRight(strip_right.Color(0, 0, 0), 0);
        }
        else if (hazardLvl == '1' && hLvlDisplay == '2') { // if strips enabled: turn off ring
          colorWipeRing(ring.Color(0, 0, 0), 0);
        }
      }
      
      setupComplete = false;
      newHazard = false;
    }
    delay(500);
  }
}


/*
   ================================ Android functions ================================
*/
void readAndroid() {
  if (Serial.available())
  {
    while (Serial.available() > 0) // Don't read unless
    {
      if (index < 32) // One less than the size of the array
      {
        inChar = Serial.read(); // Read a character
        inData[index] = inChar; // Store it
        index++;                // Increment where to write next
        inData[index] = '\0';   // Null terminate the string
      }
    }
    Serial.print("(Log) Received message: ");
    Serial.println(inData);
    Serial.println(" ");
    index = 0;
    msgProcessed = true;
    parseMSG(inData);
    processMSG(inData);
    msgProcessed = false;
  }
}

void processMSG(char* This) {

  if (This[0] == '1') { // TriggerHazard

    Serial.println("(Log) TriggerHazard");
    logMSG();
    newHazard = true;

    if (vibroHelmet_1 == '4') {
      Serial.print("Notify Helmet (other arduino) as well) - ");
      Serial.println(This);
      notify(This);
    }
  }
  else if (This[0] == '2') { // TestGadget
    // LOG
    Serial.print("(Log) Sending notification to Gadget ");
    Serial.println(This[1]);

    testCase = true;
    
    if (This[1]=='1') {
      testRing = true;
      ledRing_2 = This[2];
      ledRing_3 = This[3];
    }
    if (This[1]=='2') {
      testStrips = true;
      ledHandles_2 = This[2];
      ledHandles_3 = This[3];
    }
    if (This[1]=='3') {
      testVibroHandles = true;
      vibroHandles_2 = This[2];
      vibroHandles_3 = This[3];
    }
    

    if (This[1]=='4') {
      notify(This);
    }
  }
  else if (This[0] == '3') { //updateHazardLevel
    Serial.println("(Log) Update Hazard Level");
    Serial.print("Show Hazard Level: ");
    Serial.print(This[1]);
    Serial.print(" | Color : ");
    Serial.print(This[2]);
    Serial.print(" | Gadget : ");
    Serial.println(This[3]);
    updateHazardLevel = true;
    hazardLvl = This[1];
    hLvlColor = This[2];
    hLvlDisplay = This[3];
  }
}

void parseMSG(char* This) {

  int index = 0;

  video = ' ';
  hazard = ' ';
  sev = ' ';
  dir = ' ';
  nCars = ' ';
  nCyclists = ' ';
  nPedestrians = ' ';
  nNature = ' ';
  nTerrain = ' ';
  nBehavior = ' ';
  hazardLvl = ' ';
  hLvlDisplay = ' ';

  ledRing_1 = ' ';
  ledRing_2 = ' ';
  ledRing_3 = ' ';
  ledRing_4 = ' ';

  ledHandles_1 = ' ';
  ledHandles_2 = ' ';
  ledHandles_3 = ' ';
  ledHandles_4 = ' ';

  vibroHandles_1 = ' ';
  vibroHandles_2 = ' ';
  vibroHandles_3 = ' ';
  vibroHandles_4 = ' ';

  vibroHelmet_1 = ' ';
  vibroHelmet_2 = ' ';
  vibroHelmet_3 = ' ';
  vibroHelmet_4 = ' ';

  ringEnabled = false;
  stripsEnabled = false;
  vibroHandlesEnabled = false;

  while (index < strlen(This)) {
    if       (index == 0) {}
    else if  (index == 1) {
      video = This[index];
    }
    else if  (index == 2) {
      hazard = This[index];
    }
    else if  (index == 3) {
      sev = This[index];
    }
    else if  (index == 4) {
      dir = This[index];
    }
    else if  (index == 5) {
      nCars = This[index];
    }
    else if  (index == 6) {
      nCyclists = This[index];
    }
    else if  (index == 7) {
      nPedestrians = This[index];
    }
    else if  (index == 8) {
      nNature = This[index];
    }
    else if  (index == 9) {
      nTerrain = This[index];
    }
    else if (index == 10) {
      nBehavior = This[index];
    }
    else if (index == 11) {
      hazardLvl = This[index];
    }
    else if (index == 12) {
      hLvlDisplay = This[index];
    }

    else if (index == 13 && This[index] == '1') {
      ringEnabled = true;
      ledRing_1 = This[index];
      ledRing_2 = This[index + 1];
      ledRing_3 = This[index + 2];
      ledRing_4 = This[index + 3];
    }
    else if (index == 13 && This[index] == '2') {
      stripsEnabled = true;
      ledHandles_1 = This[index];
      ledHandles_2 = This[index + 1];
      ledHandles_3 = This[index + 2];
      ledHandles_4 = This[index + 3];
    }
    else if (index == 13 && This[index] == '3') {
      vibroHandlesEnabled = true;
      vibroHandles_1 = This[index];
      vibroHandles_2 = This[index + 1];
      vibroHandles_3 = This[index + 2];
      vibroHandles_4 = This[index + 3];
    }
    else if (index == 13 && This[index] == '4') {
      vibroHelmet_1 = This[index];
      vibroHelmet_2 = This[index + 1];
      vibroHelmet_3 = This[index + 2];
      vibroHelmet_4 = This[index + 3];
    }

    else if (index == 17 && This[index] == '2') {
      stripsEnabled = true;
      ledHandles_1 = This[index];
      ledHandles_2 = This[index + 1];
      ledHandles_3 = This[index + 2];
      ledHandles_4 = This[index + 3];
    }
    else if (index == 17 && This[index] == '3') {
      vibroHandlesEnabled = true;
      vibroHandles_1 = This[index];
      vibroHandles_2 = This[index + 1];
      vibroHandles_3 = This[index + 2];
      vibroHandles_4 = This[index + 3];
    }
    else if (index == 17 && This[index] == '4') {
      vibroHelmet_1 = This[index];
      vibroHelmet_2 = This[index + 1];
      vibroHelmet_3 = This[index + 2];
      vibroHelmet_4 = This[index + 3];
    }

    else if (index == 21 && This[index] == '3') {
      vibroHandlesEnabled = true;
      vibroHandles_1 = This[index];
      vibroHandles_2 = This[index + 1];
      vibroHandles_3 = This[index + 2];
      vibroHandles_4 = This[index + 3];
    }
    else if (index == 21 && This[index] == '4') {
      vibroHelmet_1 = This[index];
      vibroHelmet_2 = This[index + 1];
      vibroHelmet_3 = This[index + 2];
      vibroHelmet_4 = This[index + 3];
    }

    else if (index == 25 && This[index] == '4') {
      vibroHelmet_1 = This[index];
      vibroHelmet_2 = This[index + 1];
      vibroHelmet_3 = This[index + 2];
      vibroHelmet_4 = This[index + 3];
    }

    index++;
  }
}

void logMSG() {

  Serial.println("(Log) Message LOG");

  Serial.print(video);
  Serial.println(" - Video Number");

  Serial.print(hazard);
  Serial.println(" - Hazard Number");

  Serial.print(sev);
  Serial.println(" - Severity");

  Serial.print(dir);
  Serial.println(" - Direction");

  Serial.print(nCars);
  Serial.println(" - (notify about) Cars");

  Serial.print(nCyclists);
  Serial.println(" - (notify about) Cyclists");

  Serial.print(nPedestrians);
  Serial.println(" - (notify about) Pedestrians");

  Serial.print(nNature);
  Serial.println(" - (notify about) Nature");

  Serial.print(nTerrain);
  Serial.println(" - (notify about) Terrain Environment");

  Serial.print(nBehavior);
  Serial.println(" - (notify about) Road Behavior");

  Serial.print(hazardLvl);
  Serial.println(" - Show Hazard Level");

  Serial.print(hLvlDisplay);
  Serial.println(" - Hazard Level Display Method");


  Serial.print("LED Ring: ");
  Serial.print(ledRing_1);
  Serial.print(ledRing_2);
  Serial.print(ledRing_3);
  Serial.println(ledRing_4);

  Serial.print("LED Strips: ");
  Serial.print(ledHandles_1);
  Serial.print(ledHandles_2);
  Serial.print(ledHandles_3);
  Serial.println(ledHandles_4);

  Serial.print("Vibro Handles: ");
  Serial.print(vibroHandles_1);
  Serial.print(vibroHandles_2);
  Serial.print(vibroHandles_3);
  Serial.println(vibroHandles_4);

  Serial.print("Vibro Helmet: ");
  Serial.print(vibroHelmet_1);
  Serial.print(vibroHelmet_2);
  Serial.print(vibroHelmet_3);
  Serial.println(vibroHelmet_4);
  Serial.println(" ");
}

void notify(char* msg) {
  // LOG
  Serial.print("(Log) Notify Device 2: ");
  Serial.println(msg);
  Wire.beginTransmission(1);
  Wire.write(msg);
  Wire.endTransmission();
}

void scanner() {
  // LOG
  int j = 0;
  Serial.println ("(Log) I2C scanner. Scanning ...");
  for (byte i = 1; i < 5; i++) // total number of slaves being tested
  {
    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
      {
        Serial.print ("(Log) Found address: ");
        Serial.print (i, DEC);
        Serial.print (" (0x");
        Serial.print (i, HEX);
        Serial.println (")");
        j++;
      }
     delay (5);
   }
   Serial.println ("(Log) Done.");
   Serial.print ("(Log) Found ");
   Serial.print (j, DEC);
   Serial.println (" slave(s).");
}


/*
   ================================ Gadget Functions ================================
*/
// ================================================================================
// LED RING
// ================================================================================
// Fill the dots one after the other with a color
void colorWipeRing(uint32_t c, uint8_t wait) {
  for (uint16_t i = 0; i < ring.numPixels(); i++) {
    ring.setPixelColor(i, c);
    ring.show();
    delay(wait);
  }
}

// ================================================================================
// LED STRIP RIGHT
// ================================================================================
// Fill the dots one after the other with a color
void colorWipeStripRight(uint32_t c, uint8_t wait) {
  for (uint16_t i = 0; i < strip_right.numPixels(); i++) {
    strip_right.setPixelColor(i, c);
    strip_right.show();
    delay(wait);
  }
}

// ================================================================================
// LED STRIP LEFT
// ================================================================================
// Fill the dots one after the other with a color
void colorWipeStripLeft(uint32_t c, uint8_t wait) {
  for (uint16_t i = 0; i < strip_left.numPixels(); i++) {
    strip_left.setPixelColor(i, c);
    strip_left.show();
    delay(wait);
  }
}
