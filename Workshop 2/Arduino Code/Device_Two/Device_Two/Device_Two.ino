#include <Wire.h>

#define WIRE_ID 1

#define VIBRO_LEFT_FRONT 3
#define VIBRO_LEFT_CENTER 5
#define VIBRO_LEFT_BACK 6
#define VIBRO_RIGHT_FRONT 9
#define VIBRO_RIGHT_CENTER 10
#define VIBRO_RIGHT_BACK 11

// Variables to store incoming message from Android device
char inData[32];                  // Allocate some space for the string
char inChar = -1;                 // Where to store the character read
byte index = 0;                   // Index into array; where to store the character
boolean msgProcessed = false;     // Flag if the Android message was processed


// Variables to store current setup
char video;
char hazard;
char sev;
char dir;
char nCars;
char nCyclists;
char nPedestrians;
char nNature;
char nTerrain;
char nBehavior;
char hazardLvl;
char hLvlDisplay;
char hLvlColor;

char vibroHelmet_1;
char vibroHelmet_2;
char vibroHelmet_3;
char vibroHelmet_4;
int vibroHelmet_power;

int hazardDuration;


// Flags
boolean setupComplete = false;
boolean newHazard = false;
boolean testCase = false;

/*
   ============================== Arduino init functions =============================
*/
void setup() {
  Serial.begin(9600); // Start serial at 9600 baud
  Wire.begin(WIRE_ID);
  Wire.onReceive(receiveEvent);

  Serial.print("CONNECTED ON ");
  Serial.println(WIRE_ID);

  pinMode (VIBRO_LEFT_FRONT, OUTPUT);
  pinMode (VIBRO_LEFT_CENTER, OUTPUT);
  pinMode (VIBRO_LEFT_BACK, OUTPUT);
  pinMode (VIBRO_RIGHT_FRONT, OUTPUT);
  pinMode (VIBRO_RIGHT_CENTER, OUTPUT);
  pinMode (VIBRO_RIGHT_BACK, OUTPUT);
}

void loop() {

  // ================================================================================
  // TEST SETUP
  // ================================================================================
  if (testCase) {

    hazardDuration = 2; // 2 seconds

    if (vibroHelmet_3 == '1') { // pattern 1
      analogWrite( VIBRO_LEFT_FRONT   , vibroHelmet_power );
      analogWrite( VIBRO_LEFT_CENTER  , vibroHelmet_power );
      analogWrite( VIBRO_LEFT_BACK    , vibroHelmet_power );
      analogWrite( VIBRO_RIGHT_FRONT  , vibroHelmet_power );
      analogWrite( VIBRO_RIGHT_CENTER , vibroHelmet_power );
      analogWrite( VIBRO_RIGHT_BACK   , vibroHelmet_power );
      delay((hazardDuration * 1000));
      analogWrite( VIBRO_LEFT_FRONT   , 0 );
      analogWrite( VIBRO_LEFT_CENTER  , 0 );
      analogWrite( VIBRO_LEFT_BACK    , 0 );
      analogWrite( VIBRO_RIGHT_FRONT  , 0 );
      analogWrite( VIBRO_RIGHT_CENTER , 0 );
      analogWrite( VIBRO_RIGHT_BACK   , 0 );
    }
    else if (vibroHelmet_3 == '2') { // pattern 2
      for (int i = 0; i < 4; i++) {
        analogWrite( VIBRO_LEFT_FRONT   , vibroHelmet_power );
        analogWrite( VIBRO_LEFT_CENTER  , vibroHelmet_power );
        analogWrite( VIBRO_LEFT_BACK    , vibroHelmet_power );
        analogWrite( VIBRO_RIGHT_FRONT  , vibroHelmet_power );
        analogWrite( VIBRO_RIGHT_CENTER , vibroHelmet_power );
        analogWrite( VIBRO_RIGHT_BACK   , vibroHelmet_power );
        delay((hazardDuration * 1000) / 8);
        analogWrite( VIBRO_LEFT_FRONT   , 0 );
        analogWrite( VIBRO_LEFT_CENTER  , 0 );
        analogWrite( VIBRO_LEFT_BACK    , 0 );
        analogWrite( VIBRO_RIGHT_FRONT  , 0 );
        analogWrite( VIBRO_RIGHT_CENTER , 0 );
        analogWrite( VIBRO_RIGHT_BACK   , 0 );
        delay((hazardDuration * 1000) / 8);
      }
    }
    else if (vibroHelmet_3 == '3') { // pattern 3
      for (int i = 0; i < vibroHelmet_power; i++) {
        analogWrite( VIBRO_LEFT_FRONT   , i );
        analogWrite( VIBRO_LEFT_CENTER  , i );
        analogWrite( VIBRO_LEFT_BACK    , i );
        analogWrite( VIBRO_RIGHT_FRONT  , i );
        analogWrite( VIBRO_RIGHT_CENTER , i );
        analogWrite( VIBRO_RIGHT_BACK   , i );
        delay((hazardDuration * 1000) / vibroHelmet_power);
      }
      analogWrite( VIBRO_LEFT_FRONT   , 0 );
      analogWrite( VIBRO_LEFT_CENTER  , 0 );
      analogWrite( VIBRO_LEFT_BACK    , 0 );
      analogWrite( VIBRO_RIGHT_FRONT  , 0 );
      analogWrite( VIBRO_RIGHT_CENTER , 0 );
      analogWrite( VIBRO_RIGHT_BACK   , 0 );
    }
    testCase = false;
  }


  // ================================================================================
  // TRIGGER GADGET
  // ================================================================================
  if (newHazard) {
  
    // ================================================================================
    // VIDEO 1 - HAZARDS:
    // ================================================================================
    if (video == '1' && hazard == '1') {
      hazardDuration = 2; // 2 seconds
      if (nPedestrians == '1') setupComplete = true;
    }
    else if (video == '1' && hazard == '2') {
      hazardDuration = 5; // 4 seconds
      if (nCyclists == '1' || nTerrain == '1') setupComplete = true;
    }
    else if (video == '1' && hazard == '3') {
      hazardDuration = 2; // 4 seconds
      if (nPedestrians == '1') setupComplete = true;
    }
    else if (video == '1' && hazard == '4') {
      hazardDuration = 1; // 4 seconds
      if (nPedestrians == '1') setupComplete = true;
    }
    else if (video == '1' && hazard == '5') {
      hazardDuration = 1; // 4 seconds
      if (nPedestrians == '1') setupComplete = true;
    }
    else if (video == '1' && hazard == '6') {
      hazardDuration = 5; // 4 seconds
      if (nPedestrians == '1') setupComplete = true;
    }
    else if (video == '1' && hazard == '7') {
      hazardDuration = 1; // 4 seconds
      if (nNature == '1') setupComplete = true;
    }
    else if (video == '1' && hazard == '8') {
      hazardDuration = 3; // 4 seconds
      if (nPedestrians == '1') setupComplete = true;
    }
    else if (video == '1' && hazard == '9') {
      hazardDuration = 2; // 4 seconds
      if (nCars == '1') setupComplete = true;
    }
    else if (video == '1' && hazard == 'A') {
      hazardDuration = 8; // 4 seconds
      if (nCars == '1') setupComplete = true;
    }

    // ================================================================================
    // TRIGGER HAZARD
    // ================================================================================
    if (setupComplete) {
      if (vibroHelmet_4=='1' && vibroHelmet_3 == '1') {
        switch (dir) {
          case '1': analogWrite(VIBRO_LEFT_FRONT, vibroHelmet_power); analogWrite(VIBRO_RIGHT_FRONT, vibroHelmet_power); break;
          case '2': analogWrite(VIBRO_RIGHT_FRONT , vibroHelmet_power); analogWrite(VIBRO_RIGHT_CENTER, vibroHelmet_power); analogWrite(VIBRO_RIGHT_BACK, vibroHelmet_power); break;
          case '3': analogWrite(VIBRO_LEFT_BACK, vibroHelmet_power); analogWrite(VIBRO_RIGHT_BACK, vibroHelmet_power); break;
          case '4': analogWrite(VIBRO_LEFT_FRONT , vibroHelmet_power); analogWrite(VIBRO_LEFT_CENTER, vibroHelmet_power); analogWrite(VIBRO_LEFT_BACK, vibroHelmet_power); break;
          default: break;
        }
        delay((hazardDuration * 1000));
        switch (dir) {
          case '1': analogWrite(VIBRO_LEFT_FRONT, 0); analogWrite(VIBRO_RIGHT_FRONT, 0); break;
          case '2': analogWrite(VIBRO_RIGHT_FRONT , 0); analogWrite(VIBRO_RIGHT_CENTER, 0); analogWrite(VIBRO_RIGHT_BACK, 0); break;
          case '3': analogWrite(VIBRO_LEFT_BACK, 0); analogWrite(VIBRO_RIGHT_BACK, 0); break;
          case '4': analogWrite(VIBRO_LEFT_FRONT , 0); analogWrite(VIBRO_LEFT_CENTER, 0); analogWrite(VIBRO_LEFT_BACK, 0); break;
          default: break;
        }
      }
      else if (vibroHelmet_4=='1' && vibroHelmet_3 == '2') {
        for (int i = 0; i < (hazardDuration * 2); i++) {
          switch (dir) {
            case '1': analogWrite(VIBRO_LEFT_FRONT, vibroHelmet_power); analogWrite(VIBRO_RIGHT_FRONT, vibroHelmet_power); break;
            case '2': analogWrite(VIBRO_RIGHT_FRONT , vibroHelmet_power); analogWrite(VIBRO_RIGHT_CENTER, vibroHelmet_power); analogWrite(VIBRO_RIGHT_BACK, vibroHelmet_power); break;
            case '3': analogWrite(VIBRO_LEFT_BACK, vibroHelmet_power); analogWrite(VIBRO_RIGHT_BACK, vibroHelmet_power); break;
            case '4': analogWrite(VIBRO_LEFT_FRONT , vibroHelmet_power); analogWrite(VIBRO_LEFT_CENTER, vibroHelmet_power); analogWrite(VIBRO_LEFT_BACK, vibroHelmet_power); break;
            default: break;
          }
          delay(250);
          switch (dir) {
            case '1': analogWrite(VIBRO_LEFT_FRONT, 0); analogWrite(VIBRO_RIGHT_FRONT, 0); break;
            case '2': analogWrite(VIBRO_RIGHT_FRONT , 0); analogWrite(VIBRO_RIGHT_CENTER, 0); analogWrite(VIBRO_RIGHT_BACK, 0); break;
            case '3': analogWrite(VIBRO_LEFT_BACK, 0); analogWrite(VIBRO_RIGHT_BACK, 0); break;
            case '4': analogWrite(VIBRO_LEFT_FRONT , 0); analogWrite(VIBRO_LEFT_CENTER, 0); analogWrite(VIBRO_LEFT_BACK, 0); break;
            default: break;
          }
          delay(250);
        }
      }
      else if (vibroHelmet_4=='1' && vibroHelmet_3 == '3') {
        for (int i = 0; i < vibroHelmet_power; i++) {
          switch (dir) {
            case '1': analogWrite(VIBRO_LEFT_FRONT, i); analogWrite(VIBRO_RIGHT_FRONT, i); delay(((hazardDuration/2) * 1000) / vibroHelmet_power); break;
            case '2': analogWrite(VIBRO_RIGHT_FRONT , i); analogWrite(VIBRO_RIGHT_CENTER, i); analogWrite(VIBRO_RIGHT_BACK, i); delay(((hazardDuration/2) * 1000) / vibroHelmet_power); break;
            case '3': analogWrite(VIBRO_LEFT_BACK, i); analogWrite(VIBRO_RIGHT_BACK, i); delay(((hazardDuration/2) * 1000) / vibroHelmet_power); break;
            case '4': analogWrite(VIBRO_LEFT_FRONT , i); analogWrite(VIBRO_LEFT_CENTER, i); analogWrite(VIBRO_LEFT_BACK, i); delay(((hazardDuration/2) * 1000) / vibroHelmet_power); break;
            default: break;
          }
        }
        for (int i = vibroHelmet_power; i > 0; i--) {
          switch (dir) {
            case '1': analogWrite(VIBRO_LEFT_FRONT, i); analogWrite(VIBRO_RIGHT_FRONT, i); delay(((hazardDuration/2) * 1000) / vibroHelmet_power); break;
            case '2': analogWrite(VIBRO_RIGHT_FRONT , i); analogWrite(VIBRO_RIGHT_CENTER, i); analogWrite(VIBRO_RIGHT_BACK, i); delay(((hazardDuration/2) * 1000) / vibroHelmet_power); break;
            case '3': analogWrite(VIBRO_LEFT_BACK, i); analogWrite(VIBRO_RIGHT_BACK, i); delay(((hazardDuration/2) * 1000) / vibroHelmet_power); break;
            case '4': analogWrite(VIBRO_LEFT_FRONT , i); analogWrite(VIBRO_LEFT_CENTER, i); analogWrite(VIBRO_LEFT_BACK, i); delay(((hazardDuration/2) * 1000) / vibroHelmet_power); break;
            default: break;
          }
        }
      }
      else if (vibroHelmet_4=='0' && vibroHelmet_3 == '1') {
        analogWrite( VIBRO_LEFT_FRONT   , vibroHelmet_power );
        analogWrite( VIBRO_LEFT_CENTER  , vibroHelmet_power );
        analogWrite( VIBRO_LEFT_BACK    , vibroHelmet_power );
        analogWrite( VIBRO_RIGHT_FRONT  , vibroHelmet_power );
        analogWrite( VIBRO_RIGHT_CENTER , vibroHelmet_power );
        analogWrite( VIBRO_RIGHT_BACK   , vibroHelmet_power );
        delay((hazardDuration * 1000));
        analogWrite( VIBRO_LEFT_FRONT   , 0 );
        analogWrite( VIBRO_LEFT_CENTER  , 0 );
        analogWrite( VIBRO_LEFT_BACK    , 0 );
        analogWrite( VIBRO_RIGHT_FRONT  , 0 );
        analogWrite( VIBRO_RIGHT_CENTER , 0 );
        analogWrite( VIBRO_RIGHT_BACK   , 0 );
      }
      else if (vibroHelmet_4=='0' && vibroHelmet_3 == '2') {
        for (int i = 0; i < (hazardDuration * 2); i++) {
          analogWrite( VIBRO_LEFT_FRONT   , vibroHelmet_power );
          analogWrite( VIBRO_LEFT_CENTER  , vibroHelmet_power );
          analogWrite( VIBRO_LEFT_BACK    , vibroHelmet_power );
          analogWrite( VIBRO_RIGHT_FRONT  , vibroHelmet_power );
          analogWrite( VIBRO_RIGHT_CENTER , vibroHelmet_power );
          analogWrite( VIBRO_RIGHT_BACK   , vibroHelmet_power );
          delay(250);
          analogWrite( VIBRO_LEFT_FRONT   , 0 );
          analogWrite( VIBRO_LEFT_CENTER  , 0 );
          analogWrite( VIBRO_LEFT_BACK    , 0 );
          analogWrite( VIBRO_RIGHT_FRONT  , 0 );
          analogWrite( VIBRO_RIGHT_CENTER , 0 );
          analogWrite( VIBRO_RIGHT_BACK   , 0 );
          delay(250);
        }
      }
      else if (vibroHelmet_4=='0' && vibroHelmet_3 == '3') {
        for (int i = 0; i < vibroHelmet_power; i++) {
          analogWrite( VIBRO_LEFT_FRONT   , i );
          analogWrite( VIBRO_LEFT_CENTER  , i );
          analogWrite( VIBRO_LEFT_BACK    , i );
          analogWrite( VIBRO_RIGHT_FRONT  , i );
          analogWrite( VIBRO_RIGHT_CENTER , i );
          analogWrite( VIBRO_RIGHT_BACK   , i );
          delay(((hazardDuration /2) * 1000) / vibroHelmet_power);
        }
        for (int i = vibroHelmet_power; i > 0; i--) {
          analogWrite( VIBRO_LEFT_FRONT   , i );
          analogWrite( VIBRO_LEFT_CENTER  , i );
          analogWrite( VIBRO_LEFT_BACK    , i );
          analogWrite( VIBRO_RIGHT_FRONT  , i );
          analogWrite( VIBRO_RIGHT_CENTER , i );
          analogWrite( VIBRO_RIGHT_BACK   , i );
          delay(((hazardDuration/2) * 1000) / vibroHelmet_power);
        }
      }

      newHazard = false;
    }
  }
}


  /*
     ================================ Wire functions ================================
  */
  void receiveEvent(int bytes) {
    video = ' ';
    hazard = ' ';
    sev = ' ';
    dir = ' ';
    nCars = ' ';
    nCyclists = ' ';
    nPedestrians = ' ';
    nNature = ' ';
    nTerrain = ' ';
    nBehavior = ' ';
    hazardLvl = ' ';
    hLvlDisplay = ' ';

    vibroHelmet_1 = ' ';
    vibroHelmet_2 = ' ';
    vibroHelmet_3 = ' ';
    vibroHelmet_4 = ' ';

    int index = 0;
    boolean nHazard = false;
    boolean nTestCase = false;

    Serial.print("(Log) Received message: ");

    while (0 < Wire.available()) {
      char c = Wire.read();
      Serial.print(c);
      if (index == 0 && c == '1') nHazard = true;
      else if (index == 0 && c == '2') nTestCase = true;

      if (nHazard) {
        if      (index == 1) video = c;
        else if (index == 2) hazard = c;
        else if (index == 3) sev = c;
        else if (index == 4) dir = c;
        else if (index == 5) nCars = c;
        else if (index == 6) nCyclists = c;
        else if (index == 7) nPedestrians = c;
        else if (index == 8) nNature = c;
        else if (index == 9) nTerrain = c;
        else if (index == 10) nBehavior = c;
        else if (index == 11) hazardLvl = c;
        else if (index == 12) hLvlDisplay = c;
        else if (index == 13 && c == '4') vibroHelmet_1 = c;
        else if (index == 14 && vibroHelmet_1 == '4') vibroHelmet_2 = c;
        else if (index == 15 && vibroHelmet_1 == '4') vibroHelmet_3 = c;
        else if (index == 16 && vibroHelmet_1 == '4') vibroHelmet_4 = c;
        else if (index == 17 && c == '4') vibroHelmet_1 = c;
        else if (index == 18 && vibroHelmet_1 == '4') vibroHelmet_2 = c;
        else if (index == 19 && vibroHelmet_1 == '4') vibroHelmet_3 = c;
        else if (index == 20 && vibroHelmet_1 == '4') vibroHelmet_4 = c;
        else if (index == 21 && c == '4') vibroHelmet_1 = c;
        else if (index == 22 && vibroHelmet_1 == '4') vibroHelmet_2 = c;
        else if (index == 23 && vibroHelmet_1 == '4') vibroHelmet_3 = c;
        else if (index == 24 && vibroHelmet_1 == '4') vibroHelmet_4 = c;
        else if (index == 25 && c == '4') vibroHelmet_1 = c;
        else if (index == 26 && vibroHelmet_1 == '4') vibroHelmet_2 = c;
        else if (index == 27 && vibroHelmet_1 == '4') vibroHelmet_3 = c;
        else if (index == 28 && vibroHelmet_1 == '4') vibroHelmet_4 = c;
      }
      else if (nTestCase) {
        if (index == 1) vibroHelmet_1 = c;
        else if (index == 2) vibroHelmet_2 = c;
        else if (index == 3) vibroHelmet_3 = c;
        else if (index == 4) vibroHelmet_4 = c;

      }
      index++;
    }

    if (nHazard) {
      newHazard = true;
      logMSG();
    }
    else if (nTestCase) testCase = true;

    vibroHelmet_power = vibroHelmet_2 - '0'; // convert char to int
    vibroHelmet_power = (153 * vibroHelmet_power / 5); // calculate power value (153 ~ 100%)

    Serial.println(" ");
  }

  void logMSG() {

    Serial.println("(Log) Message LOG");

    Serial.print(video);
    Serial.println(" - Video Number");

    Serial.print(hazard);
    Serial.println(" - Hazard Number");

    Serial.print(sev);
    Serial.println(" - Severity");

    Serial.print(dir);
    Serial.println(" - Direction");

    Serial.print(nCars);
    Serial.println(" - (notify about) Cars");

    Serial.print(nCyclists);
    Serial.println(" - (notify about) Cyclists");

    Serial.print(nPedestrians);
    Serial.println(" - (notify about) Pedestrians");

    Serial.print(nNature);
    Serial.println(" - (notify about) Nature");

    Serial.print(nTerrain);
    Serial.println(" - (notify about) Terrain Environment");

    Serial.print(nBehavior);
    Serial.println(" - (notify about) Road Behavior");

    Serial.print(hazardLvl);
    Serial.println(" - Show Hazard Level");

    Serial.print(hLvlDisplay);
    Serial.println(" - Hazard Level Display Method");

    Serial.print("Vibro Helmet: ");
    Serial.print(vibroHelmet_1);
    Serial.print(vibroHelmet_2);
    Serial.print(vibroHelmet_3);
    Serial.println(vibroHelmet_4);
    Serial.println(" ");
  }
