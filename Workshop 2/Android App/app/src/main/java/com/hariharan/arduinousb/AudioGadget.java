package com.hariharan.arduinousb;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by Florian Wirth on 2/11/2017.
 */

public class AudioGadget {

    private MediaPlayer mediaPlayer;
    private Context c;

    public AudioGadget(Context ctx){
        //mediaPlayer = MediaPlayer.create(ctx, R.raw.test);
        c = ctx;
    }

    public void play(int file){
        switch (file) {
            case 1:
                mediaPlayer = MediaPlayer.create(c,R.raw.audio1);
                break;
            case 2:
                mediaPlayer = MediaPlayer.create(c,R.raw.test);
                break;
            case 3:
                mediaPlayer = MediaPlayer.create(c,R.raw.audio3);
                break;
            case 4:
                mediaPlayer = MediaPlayer.create(c,R.raw.audio4);
                break;
            case 5:
                mediaPlayer = MediaPlayer.create(c,R.raw.audio5);
                break;
            case 6:
                mediaPlayer = MediaPlayer.create(c,R.raw.audio6);
                break;
            default:
                break;
        }
        mediaPlayer.seekTo(0);
        mediaPlayer.start();
    }

    public void stop(){
        mediaPlayer.pause();
    }

}
