package com.hariharan.arduinousb;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Florian Wirth on 26/10/2017.
 */

public class ArduinoAdapter {

    private AppData appData;
    private String activeGadgets;
    private MainActivity mainActivity;
    private Map<String, Integer> myMap = new HashMap<String, Integer>();


    public ArduinoAdapter(MainActivity activity){
        mainActivity = activity;
        appData = mainActivity.getAppData();
        createDictionary();
    }

    public void requestUpdate() {
        String message = "s";
        if(appData.isSerialConnected())MainActivity.serialPort.write(message.getBytes());
        else mainActivity.log_buffer.append("\n(Error) Couldn't request update. Connection is closed.");
    }

    public void updateGadgets(String gadgets) {
        gadgets = gadgets.trim();
        String gadgets1[] = gadgets.split(" ");
        activeGadgets = "[";
        for (int i=0;i<gadgets1.length;i++)
        {
            for (Map.Entry<String, Integer> e : myMap.entrySet()) {
                String key = e.getKey();
                Integer value = e.getValue();
                if (value==Integer.parseInt(gadgets1[i])){
                    activeGadgets += "{\"gadget\":\""+key+"\"},";
                }
            }
        }
        activeGadgets = activeGadgets.substring(0,activeGadgets.length()-1);
        activeGadgets += "]";
    }

    public String getGadgets() {
        return activeGadgets;
    }

    public void notify(Object args) {
        try {
            Gson gson = new Gson();
            String data = gson.toJson(args);
            data = data.substring(2,data.length()-2); //remove first and last two characters
            if(appData.isSerialConnected())MainActivity.serialPort.write(data.getBytes());
            else mainActivity.log_buffer.append("\n(Error) Couldn't notify gadget. Connection is closed.");
            System.out.println(data);
        } catch (JsonSyntaxException e){
            e.printStackTrace();
        }
    }

    private void createDictionary(){
        /*myMap.put("leds01",1);
        myMap.put("leds02",2);
        myMap.put("leds03",3);
        myMap.put("leds04",4);
        myMap.put("vibro01",5);
        myMap.put("vibro02",6);
        myMap.put("vibro03",7);
        myMap.put("vibro04",8);
        myMap.put("ring01",9);
        myMap.put("lumiTape01",10);
        myMap.put("lumiTape02",11);
        myMap.put("rgbStrip01",12);
        myMap.put("rgbStrip02",13);*/
        myMap.put("rgbStrip01",1);
        myMap.put("rgbStrip02",2);
        myMap.put("rgbStrip03",3);
        myMap.put("rgbStrip04",4);
        myMap.put("vibro01",5);
        myMap.put("vibro02",6);
        myMap.put("vibro03",7);
        myMap.put("vibro04",8);
        myMap.put("ring01",9);
    }
}
