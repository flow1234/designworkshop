package com.hariharan.arduinousb;

/**
 * Created by Florian Wirth on 23/11/2017.
 */

public class AppData {

    private String logText, gadgets, ip;
    private boolean wifiConnected, serialConnected, uiEnabled, logActive;

    public AppData(){
        ip = "";
        logText = "";
        gadgets = "";
        wifiConnected = false;
        serialConnected = false;
        uiEnabled = false;
        logActive = false;
    }

    public void setIP(String i){
        ip = i;
    }

    public String getIP(){
        return ip;
    }


    public void setLogText(String s) {
        logText = s;
    }

    public String getLogText(){
        return logText;
    }


    public String getGadgets() {
        return gadgets;
    }

    public void setGadgets(String g) {
        gadgets = g;
    }


    public void setWifiConnected(boolean status){
        wifiConnected = status;
    }

    public boolean isWifiConnected(){
        return wifiConnected;
    }


    public void setSerialConnected(boolean status){
        serialConnected = status;
    }

    public boolean isSerialConnected(){
        return serialConnected;
    }


    public void setUiEnabled(boolean status) {
        uiEnabled = status;
    }

    public boolean isUiEnabled(){
        return uiEnabled;
    }


    public void setLogActive(boolean status) {
        logActive = status;
    }

    public boolean isLogActive(){
        return logActive;
    }
}
