var socket;
var vid1;
var vid2;
var currentSettings;
var gadgetSettings;

var hazards_vid1 = [
	{time:26,sent:false,severity:"2",direction:"2",txt:"vid 1 - hazard1"},
	{time:41,sent:false,severity:"4",direction:"2",txt:"vid 1 - hazard2"},
	{time:48,sent:false,severity:"4",direction:"2",txt:"vid 1 - hazard3"},
	{time:51,sent:false,severity:"4",direction:"4",txt:"vid 1 - hazard4"},
	{time:53,sent:false,severity:"4",direction:"2",txt:"vid 1 - hazard5"},
	{time:124,sent:false,severity:"4",direction:"4",txt:"vid 1 - hazard6"},
	{time:148,sent:false,severity:"4",direction:"1",txt:"vid 1 - hazard7"},
	{time:156,sent:false,severity:"4",direction:"4",txt:"vid 1 - hazard8"},
	{time:170,sent:false,severity:"4",direction:"4",txt:"vid 1 - hazard9"},
	{time:233,sent:false,severity:"4",direction:"4",txt:"vid 1 - hazard10"}
];

var levels_vid1 = [
	{time:1,sent:false,level:"3",txt:"vid 1 - level 3"},
	{time:30,sent:false,level:"1",txt:"vid 1 - level 1"},
	{time:86,sent:false,level:"3",txt:"vid 1 - level 3"},
	{time:156,sent:false,level:"1",txt:"vid 1 - level 1"}
];

var hazards_vid2 = [
	{time:3,sent:false,severity:"1",direction:"1",txt:"vid 2 - hazard1"},
	{time:4,sent:false,severity:"1",direction:"1",txt:"vid 2 - hazard2"}
];

window.onload = function() 
{
	
    socket = io();
    
    /*
    // On call from server. Update list of gadgets.
    socket.on('server push to controller', function(msg) {
        console.log(msg);
        console.log(JSON.parse(msg));
        update(JSON.parse(msg));
    });

    // On call from server. Update list of gadgets.
    socket.on('BA_update', function(msg) {
        console.log(JSON.parse(msg));
        update(JSON.parse(msg));
    });
    */

    vid1 = document.getElementById("vid1");
    vid2 = document.getElementById("vid2");
    resetSettings();
    updateSettings();
    loadVideoSettings();
}

function loadVideoSettings(){
    
    // VIDEO 1
    //
    // if the video reaches a certain time: 
    // If specified, send hazard notification
    vid1.ontimeupdate = function(){
      	for (i=0;i<Object.keys(hazards_vid1).length;i++){
	        if (vid1.currentTime.toFixed()==hazards_vid1[i].time && !hazards_vid1[i].sent){
	            console.log(hazards_vid1[i].txt);
	            console.log(currentSettings);
	            console.log(gadgetSettings);
	            console.log('');
	            var nr = ((i+1).toString());
	            if (nr=="10") nr ="A";
	            else if (nr=="11") nr="B";
	            else if (nr=="12") nr="C";
	            else if (nr=="13") nr="D";
	            else if (nr=="14") nr="E";
	            triggerHazard("1",nr,hazards_vid1[i].severity,hazards_vid1[i].direction);
	            hazards_vid1[i].sent = true; 
	        }
      	}
      	for (i=0;i<Object.keys(levels_vid1).length;i++){
      		if (vid1.currentTime.toFixed()==levels_vid1[i].time && !levels_vid1[i].sent){
      			var msg = "3"+(($('#colorcoding-radio-1').is(':checked')) ? "1" : "0")+levels_vid1[i].level+(($('#colorcoding-radio-choice-1').is(':checked')) ? "1" : "2");
      			console.log(levels_vid1[i].txt);
      			console.log(msg);
      			socket.emit('triggerHazard',msg);
      			levels_vid1[i].sent = true;
      		}
      	}
    };

    // if the play button is pressed: update the settings
    vid1.onplay = function(){
    	updateSettings();
    	updateGadgets();
    }

    // if the video is paused: Reset all notifications sent
    vid1.onpause = function(){
      	for (i=0;i<Object.keys(hazards_vid1).length;i++){
        	hazards_vid1[i].sent = false;
      	}
      	for (i=0;i<Object.keys(levels_vid1).length;i++){
      		levels_vid1[i].sent = false;
      	}
    };

    // if the video has ended: Reset all notifications sent and set the video back to the start
    vid1.onended = function(){
      	alert("Video 1 has ended");
      	for (i=0;i<Object.keys(hazards_vid1).length;i++){
        	hazards_vid1[i].sent = false;
      	}
      	for (i=0;i<Object.keys(levels_vid1).length;i++){
      		levels_vid1[i].sent = false;
      	}
      	vid1.currentTime = 0;
    };


    // VIDEO 2
    //
    // if the video reaches a certain time: 
    // If specified, send hazard notification
    vid2.ontimeupdate = function(){
      	for (i=0;i<Object.keys(hazards_vid2).length;i++){
	        if (vid2.currentTime.toFixed()==hazards_vid2[i].time && !hazards_vid2[i].sent){
	            console.log(hazards_vid2[i].txt);
	            console.log(currentSettings);
	            console.log(gadgetSettings);
	            console.log('');
	            var nr = ((i+1).toString());
	            if (nr=="10") nr ="A";
	            else if (nr=="11") nr="B";
	            else if (nr=="12") nr="C";
	            else if (nr=="13") nr="D";
	            else if (nr=="14") nr="E";
	            triggerHazard("2",nr,hazards_vid2[i].severity,hazards_vid2[i].direction);
	            hazards_vid2[i].sent = true; 
	        }
      	}
      	for (i=0;i<Object.keys(levels_vid2).length;i++){
      		if (vid2.currentTime.toFixed()==levels_vid2[i].time && !levels_vid2[i].sent){
      			var msg = "3"+(($('#colorcoding-radio-1').is(':checked')) ? "1" : "0")+levels_vid2[i].level+(($('#colorcoding-radio-choice-1').is(':checked')) ? "1" : "2");
      			console.log(levels_vid2[i].txt);
      			console.log(msg);
      			socket.emit('triggerHazard',msg);
      			levels_vid2[i].sent = true;
      		}
      	}
    };

    // if the play button is pressed: update the settings
    vid2.onplay = function(){
    	updateSettings();
    	updateGadgets();
    }

    // if the video is paused: Reset all notifications sent
    vid2.onpause = function(){
      	for (i=0;i<Object.keys(hazards_vid2).length;i++){
        	hazards_vid2[i].sent = false;
      	}
      	for (i=0;i<Object.keys(levels_vid2).length;i++){
      		levels_vid2[i].sent = false;
      	}
    };

    // if the video has ended: Reset all notifications sent and set the video back to the start
    vid2.onended = function(){
      	alert("Video 2 has ended");
      	for (i=0;i<Object.keys(hazards_vid2).length;i++){
        	hazards_vid2[i].sent = false;
      	}
      	for (i=0;i<Object.keys(levels_vid2).length;i++){
      		levels_vid2[i].sent = false;
      	}
      	vid2.currentTime = 0;
    };
} 

function resetSettings() {
  	$('.prop').each(function(i, obj) {
    	$('#'+this.id).prop('checked',true).checkboxradio('refresh');
  	});
  	$('#colorcoding-radio-2').prop('checked',false).checkboxradio('refresh');
}

function updateSettings() {
  	currentSettings = [];
  	$('.prop').each(function(i, obj) {
  		var type = $('#'+this.id).prop('name');
  		var name = $('label[for="'+this.id+'"]').html();
  		var isChecked = $('#'+this.id).prop('checked');
	    currentSettings.push({
	  		type:type,
	  		name:name,
	  		isChecked:isChecked
	  	});
  	});
  	//console.log(currentSettings);
}

function updateGadgets() {

	gadgetSettings = [];

	var ledRingEnabled = $('#gadgets-cb-1').prop('checked');
	var ledRingColor = $('#ledRing-color-select').find('option:selected').text();
	var ledRingPattern = $('#ledRing-pattern-select').find('option:selected').text();
	var ledRingDirectionEnabled = $('#g01-checkbox-enable').prop('checked');

	gadgetSettings.push({
		gadget:'led-ring',
		enabled:ledRingEnabled,
		color:ledRingColor,
		pattern:ledRingPattern,
		direction:ledRingDirectionEnabled
	});

	//console.log('LED-Ring\nEnabled: ' + ledRingEnabled + '\nColor: ' + ledRingColor + ' | Pattern: ' + ledRingPattern + ' | Direction: ' + ledRingDirectionEnabled + '\n\n');

	var ledHandlesEnabled = $('#gadgets-cb-2').prop('checked');
	var ledHandlesColor = $('#ledHandles-color-select').find('option:selected').text();
	var ledHandlesPattern = $('#ledHandles-pattern-select').find('option:selected').text();
	var ledHandlesDirectionEnabled = $('#g02-checkbox-enable').prop('checked');

	gadgetSettings.push({
		gadget:'led-handles',
		enabled: ledHandlesEnabled,
		color: ledHandlesColor,
		pattern: ledHandlesPattern,
		direction: ledHandlesDirectionEnabled
	});

	//console.log('LED-Handles\nEnabled: ' + ledHandlesEnabled + '\nColor: ' + ledHandlesColor + ' | Pattern: ' + ledHandlesPattern + ' | Direction: ' + ledHandlesDirectionEnabled + '\n\n');

	var vibroHandlesEnabled = $('#gadgets-cb-3').prop('checked');;
	var vibroHandlesPower = $('#vibroHandles-power-slider').val();
	var vibroHandlesPattern = $('#vibroHandles-pattern-select').find('option:selected').text();
	var vibroHandlesDirectionEnabled = $('#g03-checkbox-enable').prop('checked');

	gadgetSettings.push({
		gadget:'vibro-handles',
		enabled: vibroHandlesEnabled,
		power: vibroHandlesPower,
		pattern: vibroHandlesPattern,
		direction: vibroHandlesDirectionEnabled
	});

	//console.log('Vibro-Handles\nEnabled: ' + vibroHandlesEnabled + '\nPower: ' + vibroHandlesPower + ' | Pattern: ' + vibroHandlesPattern + ' | Direction: ' + vibroHandlesDirectionEnabled + '\n\n');

	var vibroHelmetEnabled = $('#gadgets-cb-4').prop('checked');
	var vibroHelmetPower = $('#vibroHelmet-power-slider').val();
	var vibroHelmetPattern = $('#vibroHelmet-pattern-select').find('option:selected').text();
	var vibroHelmetDirectionEnabled = $('#g04-checkbox-enable').prop('checked');

	gadgetSettings.push({
		gadget:'vibro-helmet',
		enabled: vibroHelmetEnabled,
		power: vibroHelmetPower,
		pattern: vibroHelmetPattern,
		direction: vibroHelmetDirectionEnabled
	});

	//console.log('Vibro-Helmet\nEnabled: ' + vibroHelmetEnabled + '\nPower: ' + vibroHelmetPower + ' | Pattern: ' + vibroHelmetPattern + ' | Direction: ' + vibroHelmetDirectionEnabled + '\n\n');

	//console.log(gadgetSettings);
}

function disableHazardLevel(){
	$('#colorcoding-radio-choice-1').prop('checked',false).attr('disabled','disabled').checkboxradio('refresh');
	$('#colorcoding-radio-choice-2').prop('checked',false).attr('disabled','disabled').checkboxradio('refresh');
}

function enableHazardLevel(){
	$('#colorcoding-radio-choice-1').prop('checked',true).removeAttr('disabled').checkboxradio('refresh');
	$('#colorcoding-radio-choice-2').prop('checked',false).removeAttr('disabled').checkboxradio('refresh');
}

function enableGadget(obj){
	switch (obj.id){
		case "gadgets-cb-1":
			if ($('#gadgets-cb-1').is(':checked')) $("#g01_item tbody").removeClass("ui-state-disabled");
			else $("#g01_item tbody").addClass("ui-state-disabled");
			break;
		case "gadgets-cb-2":
			if ($('#gadgets-cb-2').is(':checked')) $("#g02_item tbody").removeClass("ui-state-disabled");
			else $("#g02_item tbody").addClass("ui-state-disabled");
			break;
		case "gadgets-cb-3":
			if ($('#gadgets-cb-3').is(':checked')) $("#g03_item tbody").removeClass("ui-state-disabled");
			else $("#g03_item tbody").addClass("ui-state-disabled");
			break;
		case "gadgets-cb-4":
			if ($('#gadgets-cb-4').is(':checked')) $("#g04_item tbody").removeClass("ui-state-disabled");
			else $("#g04_item tbody").addClass("ui-state-disabled");
			break;
		default:
			break;
	}

}

function triggerHazard(v,h,s,d) {
	var msg = "";

	// Message Type
	var msgtype = "1";

	// Video - Settings
	var video = v;
	var hazardnumber = h;
	var severity = s;
	var direction = d;

	// Hazard - Settings
	var showCars = ($('#hazards-cb-1').is(':checked')) ? "1" : "0";
	var showCyclists = ($('#hazards-cb-2').is(':checked')) ? "1" : "0";
	var showPeds = ($('#hazards-cb-3').is(':checked')) ? "1" : "0";
	var showNature = ($('#hazards-cb-4').is(':checked')) ? "1" : "0";
	var showTerrain = ($('#hazards-cb-5').is(':checked')) ? "1" : "0";
	var showInfrastructure = ($('#hazards-cb-6').is(':checked')) ? "1" : "0";

	// Hazard Level
	var showHazardLevel = ($('#colorcoding-radio-1').is(':checked')) ? "1" : "0";
	var hazardLevelGadget = ($('#colorcoding-radio-choice-1').is(':checked')) ? "1" : "2";

	msg += msgtype;
	msg += video+hazardnumber;
	msg += severity+direction;
	msg += showCars+showCyclists+showPeds+showNature+showTerrain+showInfrastructure;
	msg += showHazardLevel+hazardLevelGadget;

	// Gadgets
	// If the gadget is enabled and not used to display hazard level
	if ($('#gadgets-cb-1').is(':checked') && !(showHazardLevel=="1"&&hazardLevelGadget=="1")) {
		var g_id = "1";
		var color = $('#ledRing-color-select').val();
		var pattern = $('#ledRing-pattern-select').val();
		var direction = ($('#g01-checkbox-enable').is(':checked')) ? "1" : "0"; 
		msg += g_id+color+pattern+direction;
	}

	if ($('#gadgets-cb-2').is(':checked') && !(showHazardLevel=="1"&&hazardLevelGadget=="2")) {
		var g_id = "2";
		var color = $('#ledHandles-color-select').val();
		var pattern = $('#ledHandles-pattern-select').val();
		var direction = ($('#g02-checkbox-enable').is(':checked')) ? "1" : "0"; 
		msg += g_id+color+pattern+direction;
	}

	if ($('#gadgets-cb-3').is(':checked')) {
		var g_id = "3";
		var color = $('#vibroHandles-power-slider').val();
		var pattern = $('#vibroHandles-pattern-select').val();
		var direction = ($('#g03-checkbox-enable').is(':checked')) ? "1" : "0"; 
		msg += g_id+color+pattern+direction;
	}

	if ($('#gadgets-cb-4').is(':checked')) {
		var g_id = "4";
		var color = $('#vibroHelmet-power-slider').val();
		var pattern = $('#vibroHelmet-pattern-select').val();
		var direction = ($('#g04-checkbox-enable').is(':checked')) ? "1" : "0"; 
		msg += g_id+color+pattern+direction;
	}

	//console.log(msg);
	socket.emit('triggerHazard',msg);
}

var i = 1;
function test(obj){
	//console.log(obj.id);
	//triggerHazard("1","1","1","2");

	socket.emit('triggerHazard',"24212");
	
/*
	switch (i){
		case 1:
			socket.emit('triggerHazard',"312");
			i=2;
			break;
		case 2:
			socket.emit('triggerHazard',"321");
			i=3;
			break;
		case 3:
			socket.emit('triggerHazard',"332");
			i=4;
			break;
		case 4:
			socket.emit('triggerHazard',"342");
			i=1;
			break;
		default:
			break;		
	*/
}

function test1(){
	var msg = "";
	var type = "2";
	var id = "1";
	var color = $('#ledRing-color-select').val();
	var pattern = $('#ledRing-pattern-select').val();
	var direction = ($('#g01-checkbox-enable').is(':checked')) ? "1" : "0"; 

	msg += type + id + color + pattern + direction;
	console.log(msg);
	socket.emit('triggerHazard',msg);
}

function test2(){
	var msg = "";
	var type = "2";
	var id = "2";
	var color = $('#ledHandles-color-select').val();
	var pattern = $('#ledHandles-pattern-select').val();
	var direction = ($('#g02-checkbox-enable').is(':checked')) ? "1" : "0"; 
	
	msg += type + id + color + pattern + direction;
	console.log(msg);
	socket.emit('triggerHazard',msg);
}

function test3(){
	var msg = "";
	var type = "2";
	var id = "3";
	var color = $('#vibroHandles-power-slider').val();
	var pattern = $('#vibroHandles-pattern-select').val();
	var direction = ($('#g03-checkbox-enable').is(':checked')) ? "1" : "0"; 
	
	msg += type + id + color + pattern + direction;
	console.log(msg);
	socket.emit('triggerHazard',msg);
}

function test4(){
	var msg = "";
	var type = "2";
	var id = "4";
	var color = $('#vibroHelmet-power-slider').val();
	var pattern = $('#vibroHelmet-pattern-select').val();
	var direction = ($('#g04-checkbox-enable').is(':checked')) ? "1" : "0"; 
	
	msg += type + id + color + pattern + direction;
	console.log(msg);
	socket.emit('triggerHazard',msg);
}