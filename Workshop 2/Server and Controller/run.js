var express = require('express');
var app     = express();
var http    = require('http').Server(app);
var io      = require('socket.io')(http);

app.use(express.static('Controller'));

io.on('connection', function(socket)
{    
    console.log('controller connected');

    socket.on('triggerHazard', function(msg){
      console.log(msg);
      io.emit("BC_update",msg);
    });

    socket.on('testGadget', function(msg){
      console.log(msg);
    });
});

http.listen(3000, function() {
  console.log('listening on *:3000');
});